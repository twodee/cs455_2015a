#version 120

varying vec3 fnormal;

const float diffuse_weight = 0.7;

const vec3 light_color = vec3(1.0);
const vec3 light_direction = normalize(vec3(0.0, 1.0, 0.0));
const vec3 albedo = vec3(1.0, 0.5, 0.0);

void main() {
  vec3 normal = normalize(fnormal);
  float litness = max(dot(normal, light_direction), 0.0);
  vec3 diffuse_color = litness * light_color * albedo;
  vec3 ambient_color = light_color * albedo;
  vec3 color = diffuse_weight * diffuse_color + (1.0 - diffuse_weight) * ambient_color;
  gl_FragColor = vec4(color, 1.0);
}

// fifth
