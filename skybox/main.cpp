#include <cmath>
#include <iostream>
#include <fstream>

#include "BaseRenderer.h"
#include "Camera.h"
#include "glut_utilities.h"
#include "Heightmap.h"
#include "Image.h"
#include "Matrix4.h"
#include "ObjUtilities.h"
#include "ObjUtilities.h"
#include "VertexArray.h"

using namespace td;

/* ------------------------------------------------------------------------- */

class SkyboxRenderer : public BaseRenderer {
  public:
    void OnDraw(float delta);
    void OnInitialize();
    void OnKey(Keys::key_t key);
    void OnSizeChanged(int width, int height);
    void OnMouseHover(int x, int y);
    void OnMousePass(bool is_entering);

  private:
    td::Heightmap *map;
    VertexAttributes *attributes[2];
    ShaderProgram *shader_programs[2];
    VertexArray *terrain;
    VertexArray *skybox;
    Texture *skybox_texture;
    Camera camera;
    Matrix4 light_model;
    int mouse_at[2];
};

/* ------------------------------------------------------------------------- */

void SkyboxRenderer::OnDraw(float delta) {
  BaseRenderer::OnDraw(delta);

  shader_programs[0]->Bind();

  // Floor
  shader_programs[0]->SetUniform("albedo", 0.3f, 1.0f, 0.3f);
  shader_programs[0]->SetUniform("modelview", camera.GetViewMatrix() * Matrix4::GetIdentity());

  terrain->Bind();
  terrain->DrawIndexed(GL_TRIANGLES);
  terrain->Unbind();

  shader_programs[0]->Unbind();

  // Draw skybox
  shader_programs[1]->Bind();

  // TODO: In model space, the cube is centered around (0, 0, 0). We want it
  // centered around the camera. We can do this by translating it first to
  // the camera's from position.
  shader_programs[1]->SetUniform("modelview", camera.GetViewMatrix() * Matrix4::GetTranslate(camera.GetFrom()));
  shader_programs[1]->SetUniform("skybox_texture", *skybox_texture);

  skybox->Bind();
  skybox->DrawIndexed(GL_TRIANGLES);
  skybox->Unbind();

  shader_programs[1]->Unbind();
}

/* ------------------------------------------------------------------------- */

void SkyboxRenderer::OnInitialize() {
  SetBackgroundColor(30.0f / 255, 150.0f / 255, 250.0f / 255, 1.0f);
  glLineWidth(5.0f);
  glPointSize(5.0f);

  glDepthFunc(GL_LEQUAL);
  glEnable(GL_DEPTH_TEST);
  shader_programs[0] = ShaderProgram::FromFiles(SHADERS_DIR "/v.glsl", SHADERS_DIR "/f.glsl");

  // Heightmap
  int nvertices;
  Vector4 *positions;
  Vector4 *normals;
  int nfaces;
  int *faces;
  map = new Heightmap(MODELS_DIR "/noise.pgm", 10.0f);
  map->ToAttributes(nvertices, positions, normals, nfaces, faces);

  attributes[0] = new VertexAttributes();
  attributes[0]->AddAttribute("position", nvertices, 4, &positions[0][0]);
  attributes[0]->AddAttribute("normal", nvertices, 4, &normals[0][0]);
  attributes[0]->AddIndices(nfaces * 3, faces);

  terrain = new VertexArray(*shader_programs[0], *attributes[0]);

  delete[] positions;
  delete[] normals;
  delete[] faces;

  // TODO: Upload our skybox geometry and shader.
  attributes[1] = ObjUtilities::Read(MODELS_DIR "/skybox.obj");
  shader_programs[1] = ShaderProgram::FromFiles(SHADERS_DIR "/skybox.v.glsl", SHADERS_DIR "/skybox.f.glsl");
  skybox = new VertexArray(*shader_programs[1], *attributes[1]);

#define SKYBOX "hell"
  Image *images[] = {
    new Image(MODELS_DIR "/" SKYBOX "_left.ppm"),
    new Image(MODELS_DIR "/" SKYBOX "_right.ppm"),
    new Image(MODELS_DIR "/" SKYBOX "_bottom.ppm"),
    new Image(MODELS_DIR "/" SKYBOX "_top.ppm"),
    new Image(MODELS_DIR "/" SKYBOX "_back.ppm"),
    new Image(MODELS_DIR "/" SKYBOX "_front.ppm")
  };
  skybox_texture = new Texture();
  skybox_texture->Wrap(Texture::CLAMP_TO_EDGE);
  glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  skybox_texture->Upload(images[0]->GetWidth(), images[0]->GetHeight(), images[0]->GetPixels(), images[1]->GetPixels(), images[2]->GetPixels(), images[3]->GetPixels(), images[4]->GetPixels(), images[5]->GetPixels());

  // TODO: Add texture.
  for (int i = 0; i < 6; ++i) {
    delete images[i];
  }

  int midx = map->GetWidth() / 2;
  int midz = map->GetDepth() / 2;
  float midy = (*map)(midx, midz);

  camera.LookAt(Vector4(midx, midy + 2.0f, midz, 1.0f), 
                Vector4(midx, midy + 2.0f, midz - 10.0f, 1.0f),
                Vector4(0.0f, 1.0f, 0.0f, 0.0f));

  OpenGL::CheckError("finished oninitialize");
}

/* ------------------------------------------------------------------------- */

void SkyboxRenderer::OnSizeChanged(int width, int height) {
  BaseRenderer::OnSizeChanged(width, height);

  Matrix4 projection = Matrix4::GetPerspective(45.0f, GetAspectRatio(), 0.1f, 400.0f);

  shader_programs[0]->Bind();
  shader_programs[0]->SetUniform("projection", projection);
  shader_programs[0]->Unbind();  
  // TODO: Uncomment after shader_programs[1] exists.
  shader_programs[1]->Bind();
  shader_programs[1]->SetUniform("projection", projection);
  shader_programs[1]->Unbind();  

  OpenGL::CheckError("on size changed");
}

/* ------------------------------------------------------------------------- */

void SkyboxRenderer::OnKey(Keys::key_t key) {
  switch (key) {
    case '|':
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      break;
    case '.':
      glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
      break;
    case '3':
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    case 'w':
      camera.Advance(1.0f);
      camera.SetHeight(map->Interpolate(camera.GetFrom()[0], camera.GetFrom()[2]) + 2.0f);
      break;
    case 's':
      camera.Advance(-1.0f);
      camera.SetHeight(map->Interpolate(camera.GetFrom()[0], camera.GetFrom()[2]) + 2.0f);
      break;
    case 'd':
      camera.Strafe(1.0f);
      camera.SetHeight(map->Interpolate(camera.GetFrom()[0], camera.GetFrom()[2]) + 2.0f);
      break;
    case 'a':
      camera.Strafe(-1.0f);
      camera.SetHeight(map->Interpolate(camera.GetFrom()[0], camera.GetFrom()[2]) + 2.0f);
      break;
    case 'q':
      camera.Yaw(3.0f);
      break;
    case 'e':
      camera.Yaw(-3.0f);
      break;
    case '[':
      camera.Roll(-3.0f);
      break;
    case ']':
      camera.Roll(3.0f);
      break;
    case '{':
      camera.Pitch(-3.0f);
      break;
    case '}':
      camera.Pitch(3.0f);
      break;
    default:
      BaseRenderer::OnKey(key);
      break;  
  }
}

/* ------------------------------------------------------------------------- */

void SkyboxRenderer::OnMouseHover(int x, int y) {
  if (mouse_at[0] != -1) {
    int diff_x = x - mouse_at[0];
    camera.Yaw(diff_x / -2.0f);
  }
  
  if (mouse_at[1] != -1) {
    int diff_y = y - mouse_at[1];
    camera.Pitch(diff_y / 10.0f);
  }
  mouse_at[0] = x;
  mouse_at[1] = y;
}

/* ------------------------------------------------------------------------- */

void SkyboxRenderer::OnMousePass(bool is_entering) {
  if (is_entering) {
    mouse_at[0] = mouse_at[1] = -1;
  } 
}

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  SkyboxRenderer *renderer = new SkyboxRenderer();
  glut_render(renderer); 
  return 0;
}

/* ------------------------------------------------------------------------- */

// skybox
