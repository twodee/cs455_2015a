add_definitions(-DSHADERS_DIR="${CMAKE_CURRENT_SOURCE_DIR}")
add_executable(skybox main.cpp ../libtwodee/Heightmap.cpp ${COMMON})
target_link_libraries(skybox ${GLUT_LIBRARIES} ${GLEW_LIBRARY} ${OPENGL_LIBRARIES})

if(WIN32)
  add_custom_command(
      TARGET skybox POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy_if_different
      "${GLUT_DLL}"
      $<TARGET_FILE_DIR:skybox>)
    
  add_custom_command(
      TARGET skybox POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy_if_different
      "${GLEW_DLL}"
      $<TARGET_FILE_DIR:skybox>)
endif(WIN32)
