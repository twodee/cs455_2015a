#version 120

uniform samplerCube skybox_texture;

varying vec3 ftexcoord;

void main() {
  gl_FragColor = vec4(textureCube(skybox_texture, ftexcoord).rgb, 1.0);
}

// skybox
