#version 120

attribute vec4 position;
attribute vec2 texcoord;

uniform mat4 projection;
uniform mat4 modelview;

varying vec2 ftexcoord;

void main() {
  vec4 position_eye = modelview * position;
  gl_Position = projection * position_eye;
  ftexcoord = texcoord;
}

// noise
