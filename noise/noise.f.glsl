#version 120

uniform sampler2D noise_texture;

varying vec2 ftexcoord;

void main() {
  // Wood Grain
  /* float d = distance(ftexcoord, vec2(0.5)); */
  /* float noise = texture2D(noise_texture, ftexcoord).r * 4.0; */
  /* float perturbation = abs(sin(d * 50.0 + noise)); */
  /* vec3 grain = mix(vec3(0.26, 0.2, 0.13), vec3(0.94, 0.72, 0.43), perturbation); */

  // Marble
  float d = ftexcoord.x + ftexcoord.y;
  float noise = texture2D(noise_texture, ftexcoord).r * 25.0;
  float perturbation = abs(sin(d * 50.0 + noise));
  vec3 grain = mix(vec3(0.04, 0.13, 0.22), vec3(1.0), perturbation);

  gl_FragColor = vec4(grain, 1.0);
}

// noise
