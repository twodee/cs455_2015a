#include <iostream>
#include "Field2.h"

int main(int argc, char **argv) {
  int width = atoi(argv[1]);
  int height = atoi(argv[2]);
  int noctaves = atoi(argv[3]);
  
  Field2 *noise = Field2::GetSmoothNoise(width, height, noctaves);
  noise->WriteToPGM(argv[4]);
  delete noise;

  return 0;
}
