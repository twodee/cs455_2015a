#version 120

uniform vec3 albedo;
const vec3 light_direction = normalize(vec3(4.0, 1.0, 0.0));

varying vec3 fnormal;
varying vec3 fposition_eye;

const vec3 diffuse_color = vec3(1.0);
const vec3 specular_color = vec3(1.0);
const float diffuse_weight = 0.9;
const float shininess = 30.0;

void main() {
  vec3 normal = normalize(fnormal);

  // Diffuse
  float n_dot_l = max(dot(normal, light_direction), 0.0);
  vec3 diffuse = diffuse_weight * n_dot_l * albedo; 

  // Ambient
  vec3 ambient = (1.0 - diffuse_weight) * albedo;

  // Specular
  vec3 halfway = normalize(light_direction - normalize(fposition_eye));
  float n_dot_h = max(dot(normal, halfway), 0.0) * step(0.0, dot(normal, light_direction));
  vec3 specular = pow(n_dot_h, shininess) * specular_color;

  gl_FragColor = vec4(diffuse + ambient + specular, 1.0);
}

// heightmap
