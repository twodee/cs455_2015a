#version 120

uniform sampler3D noise_texture;
uniform float time;

varying vec2 ftexcoord;

void main() {
  float noise = texture3D(noise_texture, vec3(ftexcoord, time)).r;
  gl_FragColor = vec4(noise + 0.2);
}

// noise
