#include <iostream>
#include <fstream>

#include "BaseRenderer.h"
#include "glut_utilities.h"
#include "Matrix4.h"
#include "VertexArray.h"

using namespace td;

/* ------------------------------------------------------------------------- */

void read(const std::string& path,
          int &npositions,
          float *&positions,
          int &nfaces,
          int *&faces) {
  std::ifstream in(path.c_str()); 
 
  std::string line;
  std::string token; 

  npositions = 0;
  nfaces = 0;

  while (in >> token) {
    if (token == "v") {
      ++npositions;
    } else if (token == "f") {
      ++nfaces;
    }
    getline(in, line);
  }

  std::cout << "npositions: " << npositions << std::endl;
  std::cout << "nfaces: " << nfaces << std::endl;

  positions = new float[npositions * 3];
  faces = new int[nfaces * 3];

  in.clear();
  in.seekg(0, std::ios_base::beg);

  float *position = positions;
  int *face = faces;
  while (in >> token) {
    if (token == "v") {
      in >> position[0] >> position[1] >> position[2]; 
      position += 3;
    } else if (token == "f") {
      in >> face[0] >> face[1] >> face[2]; 
      face[0] -= 1;
      face[1] -= 1;
      face[2] -= 1;
      face += 3;
    }
    getline(in, line);
  }

  in.close();
}

/* ------------------------------------------------------------------------- */

class FourthRenderer : public BaseRenderer {
  public:
    void OnDraw(float delta);
    void OnInitialize();
    void OnKey(Keys::key_t key);
    void OnLeftMouseDown(int x, int y);
    void OnLeftMouseDragged(int x, int y);
    void OnRightMouseDown(int x, int y);
    void OnRightMouseDragged(int x, int y);
    void OnMiddleMouseDown(int x, int y);
    void OnMiddleMouseDragged(int x, int y);

  private:
    VertexAttributes *plane_attributes;
    ShaderProgram *shader_program;
    VertexArray *plane;
    float point_size;
    int mouse_x;
    int mouse_y;

    float theta;
    Vector4 shift;
    Vector4 scale;
};

/* ------------------------------------------------------------------------- */

void FourthRenderer::OnLeftMouseDown(int x, int y) {
  mouse_x = x;
  mouse_y = y; 
}

/* ------------------------------------------------------------------------- */

void FourthRenderer::OnLeftMouseDragged(int x, int y) {
  theta += (x - mouse_x) * 0.2f;
  mouse_x = x;
  mouse_y = y;
}

/* ------------------------------------------------------------------------- */

void FourthRenderer::OnRightMouseDown(int x, int y) {
  mouse_x = x;
  mouse_y = y; 
}

/* ------------------------------------------------------------------------- */

void FourthRenderer::OnRightMouseDragged(int x, int y) {
  shift[0] += 2.0f * (x - mouse_x) / GetWidth();
  shift[1] += 2.0f * (y - mouse_y) / GetHeight();
  mouse_x = x;
  mouse_y = y;
}

/* ------------------------------------------------------------------------- */

void FourthRenderer::OnMiddleMouseDown(int x, int y) {
  mouse_x = x;
  mouse_y = y; 
}

/* ------------------------------------------------------------------------- */

void FourthRenderer::OnMiddleMouseDragged(int x, int y) {
  scale[0] += 2.0f * (x - mouse_x) / GetWidth();
  scale[1] += 2.0f * (y - mouse_y) / GetHeight();
  mouse_x = x;
  mouse_y = y;
}

/* ------------------------------------------------------------------------- */

void FourthRenderer::OnDraw(float delta) {
  BaseRenderer::OnDraw(delta);

  shader_program->Bind();  
  Matrix4 m = Matrix4::GetRotateZ(theta) * Matrix4::GetScale(scale) * Matrix4::GetTranslate(shift);
  shader_program->SetUniform("xform", m);
  /* shader_program->SetUniform("theta", theta * 3.14159f / 180.0f); */
  plane->Bind();
  plane->DrawIndexed(GL_TRIANGLES);
  plane->Unbind();
  shader_program->Unbind();
}

/* ------------------------------------------------------------------------- */

void FourthRenderer::OnInitialize() {
  SetBackgroundColor(0.0f / 255, 0.0f / 255, 0.0f / 255, 1.0f);   
  glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  point_size = 1.0f;

  theta = 0.0f;
  shift = Vector4(0.0f);
  scale = Vector4(1.0f);

  int npositions;
  int nfaces;
  int *faces;
  float *positions;
  read(MODELS_DIR "/suzanne.obj", npositions, positions, nfaces, faces);

  // Ship positions off to GPU.
  plane_attributes = new VertexAttributes();
  plane_attributes->AddAttribute("position", npositions, 3, positions);
  plane_attributes->AddIndices(nfaces * 3, faces);

  // CPU positions are free to go now.
  delete[] positions;
  delete[] faces;

  shader_program = ShaderProgram::FromFiles(SHADERS_DIR "/v.glsl", SHADERS_DIR "/f.glsl");

  plane = new VertexArray(*shader_program, *plane_attributes);
}

/* ------------------------------------------------------------------------- */

void FourthRenderer::OnKey(Keys::key_t key) {
  switch (key) {
    case 'w':
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      break;
    case 'W':
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    case '+':
      point_size += 1.0f;
      glPointSize(point_size);
      break;
    case '-':
      point_size -= 1.0f;
      glPointSize(point_size);
      break;
    case '[':
      theta += 1;
      break;
    case ']':
      theta -= 1;
      break;
    default:
      BaseRenderer::OnKey(key);
      break;  
  }
}

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  FourthRenderer *renderer = new FourthRenderer();
  glut_render(renderer); 
  return 0;
}

/* ------------------------------------------------------------------------- */

// fourth
