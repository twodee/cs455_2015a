#version 120

attribute vec3 position;

/* uniform float theta; */
uniform mat4 xform;

void main() {
  gl_Position = xform * vec4(position, 1.0);
}

// fourth
