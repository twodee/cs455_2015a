#include <cmath>
#include <iostream>
#include <fstream>

#include "BaseRenderer.h"
#include "Camera.h"
#include "FramebufferObject.h"
#include "glut_utilities.h"
#include "Heightmap.h"
#include "Image.h"
#include "Matrix4.h"
#include "ObjUtilities.h"
#include "VertexArray.h"
#define SIZE 1024

using namespace td;

/* ------------------------------------------------------------------------- */

class ShadowRenderer : public BaseRenderer {
  public:
    void OnDraw(float delta);
    void OnInitialize();
    void OnKey(Keys::key_t key);
    void OnSizeChanged(int width, int height);
    void OnMouseHover(int x, int y);
    void OnMousePass(bool is_entering);

  private:
    VertexAttributes *attributes[4];
    ShaderProgram *shader_programs[3];

    VertexArray *terrain;
    VertexArray *ball;
    VertexArray *cone;
    VertexArray *shadow_terrain;
    VertexArray *shadow_ball;

    Texture *light_depth_texture;
    Camera camera;
    Camera light_camera;
    FramebufferObject *light_depth_fbo;
    int mouse_at[2];
    Heightmap *map;
    Vector4 ball_positions[3];
};

/* ------------------------------------------------------------------------- */

void ShadowRenderer::OnDraw(float delta) {
  /* glEnable(GL_CULL_FACE); */

  static float angle = 0.0f;
  angle += delta * 20.0f;;
  if (angle > 360.0f) {
    angle -= 360.0f;
  }

  Matrix4 light_projection = Matrix4::GetPerspective(80.0f, 1.0f, 1.0f, 100.0f);
  int midx = map->GetWidth() / 2;
  int midz = map->GetDepth() / 2;
  float midy = (*map)(midx, midz);
  Matrix4 rotate = Matrix4::GetTranslate(Vector4(midx, 0.0f, midz)) *
                   Matrix4::GetRotate(angle, Vector4(0.0f, 1.0f, 0.0f)) *
                   Matrix4::GetTranslate(Vector4(-midx, 0.0f, -midz));

  // --------------------------------------------------------------------------
  // TODO draw in depth texture from POV of light
 
  light_depth_fbo->Bind();
  glReadBuffer(GL_NONE);
  glDrawBuffer(GL_NONE);
  glViewport(0, 0, light_depth_fbo->GetWidth(), light_depth_fbo->GetHeight());
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glCullFace(GL_FRONT);

  shader_programs[2]->Bind();
  shader_programs[2]->SetUniform("projection", light_projection);

  shader_programs[2]->SetUniform("modelview", light_camera.GetViewMatrix());
  shadow_terrain->Bind();
  shadow_terrain->DrawIndexed(GL_TRIANGLES);
  shadow_terrain->Unbind();

  ball->Bind();
  for (int i = 0; i < 3; ++i) {
    Matrix4 ball_to_world = rotate * Matrix4::GetTranslate(ball_positions[i]);
    shader_programs[2]->SetUniform("modelview", light_camera.GetViewMatrix() * ball_to_world);
    ball->DrawIndexed(GL_TRIANGLES);
  }
  ball->Unbind();

  shader_programs[2]->Unbind();
  light_depth_fbo->Unbind();
  OpenGL::CheckError("after fbo");

  // --------------------------------------------------------------------------
  BaseRenderer::OnDraw(delta);

  glCullFace(GL_BACK);

  shader_programs[0]->Bind();
  Vector4 light_position_eye = camera.GetViewMatrix() * light_camera.GetFrom();
  shader_programs[0]->SetUniform("light_position_eye", light_position_eye[0], light_position_eye[1], light_position_eye[2]);
  shader_programs[0]->SetUniform("light_depth_texture", *light_depth_texture);
  light_depth_texture->Bind();

  Matrix4 world_to_tex = Matrix4::GetTranslate(Vector4(0.5f, 0.5f, 0.5f)) *
                         Matrix4::GetScale(Vector4(0.5f, 0.5f, 0.5f)) *
                         light_projection *
                         light_camera.GetViewMatrix();

  shader_programs[0]->SetUniform("modelview", camera.GetViewMatrix());
  shader_programs[0]->SetUniform("object_to_tex", world_to_tex * Matrix4::GetIdentity());
  shader_programs[0]->SetUniform("albedo", 0.2f, 0.8f, 0.2f);
  terrain->Bind();
  terrain->DrawIndexed(GL_TRIANGLES);
  terrain->Unbind();

  shader_programs[0]->SetUniform("albedo", 0.0f, 0.8f, 0.9f);
  ball->Bind();
  for (int i = 0; i < 3; ++i) {
    Matrix4 ball_to_world = rotate * Matrix4::GetTranslate(ball_positions[i]);
    shader_programs[0]->SetUniform("modelview", camera.GetViewMatrix() * ball_to_world);
    shader_programs[0]->SetUniform("object_to_tex", world_to_tex * ball_to_world);
    ball->DrawIndexed(GL_TRIANGLES);
  }
  ball->Unbind();

  shader_programs[0]->Unbind();

  // --------------------------------------------------------------------------
  shader_programs[1]->Bind();
  shader_programs[1]->SetUniform("modelview", camera.GetViewMatrix() * light_camera.GetViewMatrix().GetTranspose());

  cone->Bind();
  cone->DrawIndexed(GL_TRIANGLES);
  cone->Unbind();

  shader_programs[1]->Unbind();

  OpenGL::CheckError("on draw finished");
}

/* ------------------------------------------------------------------------- */

void ShadowRenderer::OnInitialize() {
  SetBackgroundColor(0.2f, 0.0f, 0.4f, 1.0f);
  glLineWidth(5.0f);
  glPointSize(5.0f);

  glDepthFunc(GL_LEQUAL);
  glEnable(GL_DEPTH_TEST);

  shader_programs[0] = ShaderProgram::FromFiles(SHADERS_DIR "/v.glsl",
                                                SHADERS_DIR "/f.glsl");
  shader_programs[1] = ShaderProgram::FromFiles(SHADERS_DIR "/spot.v.glsl",
                                                SHADERS_DIR "/spot.f.glsl");
  shader_programs[2] = ShaderProgram::FromFiles(SHADERS_DIR "/from_light.v.glsl",
                                                SHADERS_DIR "/from_light.f.glsl");

  // Heightmap
  int nvertices;
  Vector4 *positions;
  Vector4 *normals;
  int nfaces;
  int *faces;
  map = new Heightmap(MODELS_DIR "/noise.pgm", 10.0f);
  map->ToAttributes(nvertices, positions, normals, nfaces, faces);
  attributes[0] = new VertexAttributes();
  attributes[0]->AddAttribute("position", nvertices, 4, &positions[0][0]);
  attributes[0]->AddAttribute("normal", nvertices, 4, &normals[0][0]);
  attributes[0]->AddIndices(nfaces * 3, faces);
  delete[] positions;
  delete[] normals;
  delete[] faces;

  terrain = new VertexArray(*shader_programs[0], *attributes[0]);
  shadow_terrain = new VertexArray(*shader_programs[2], *attributes[0]);

  attributes[1] = ObjUtilities::Read(MODELS_DIR "/unit_sphere.obj");
  ball = new VertexArray(*shader_programs[0], *attributes[1]);
  shadow_ball = new VertexArray(*shader_programs[2], *attributes[1]);

  attributes[2] = ObjUtilities::Read(MODELS_DIR "/cone.obj");
  cone = new VertexArray(*shader_programs[1], *attributes[2]);

  // --------------------------------------------------------------------------
  // TODO generate texture
  light_depth_texture = new Texture(); 
  light_depth_texture->Channels(Texture::DEPTH);
  light_depth_texture->Wrap(Texture::CLAMP_TO_BORDER);
  light_depth_texture->Allocate(SIZE, SIZE);
  /* glTexParameteri(GL_TEXTURE_2D, GL_DEPTH_TEXTURE_MODE, GL_INTENSITY); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_NONE); */
  /* glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL); */
  float border[] = {1.0f, 0.0f, 0.0f, 0.0f};
  glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, border);

  // --------------------------------------------------------------------------
  // TODO generate FBO
  light_depth_fbo = new FramebufferObject(light_depth_texture); 
 
  int midx = map->GetWidth() / 2;
  int midz = map->GetDepth() / 2;
  float midy = (*map)(midx, midz);

  camera.LookAt(Vector4(midx, midy + 2.0f, midz, 1.0f), 
                Vector4(midx, midy + 2.0f, midz - 10.0f, 1.0f),
                Vector4(0.0f, 1.0f, 0.0f, 0.0f));

  light_camera.LookAt(Vector4(midx, 20.0f, midz),
                      Vector4(midx, 0.0f, midz),
                      Vector4(0.0f, 0.0f, 1.0f, 0.0f));

  ball_positions[0] = Vector4(midx - 5, (*map)(midx - 5, midz + 2) + 7.0f, midz + 2);
  ball_positions[1] = Vector4(midx + 3, (*map)(midx + 3, midz - 4) + 5.0f, midz + 4);
  ball_positions[2] = Vector4(midx - 1.9, (*map)(midx - 1.9, midz + 3) + 8.9f, midz + 3);

  OpenGL::CheckError("finished oninitialize");
}

/* ------------------------------------------------------------------------- */

void ShadowRenderer::OnSizeChanged(int width, int height) {
  BaseRenderer::OnSizeChanged(width, height);

  Matrix4 projection = Matrix4::GetPerspective(45.0f, GetAspectRatio(), 0.1f, 400.0f);

  for (int i = 0; i < 2; ++i) {
    shader_programs[i]->Bind();
    shader_programs[i]->SetUniform("projection", projection);
    shader_programs[i]->Unbind();  
  }

  OpenGL::CheckError("on size changed");
}

/* ------------------------------------------------------------------------- */

void ShadowRenderer::OnKey(Keys::key_t key) {
  switch (key) {
    case Keys::LEFT:
      light_camera.Yaw(3.0f);
      break;
    case Keys::RIGHT:
      light_camera.Yaw(-3.0f);
      break;
    case Keys::UP:
      light_camera.Pitch(3.0f);
      break;
    case Keys::DOWN:
      light_camera.Pitch(-3.0f);
      break;
    case '|':
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      break;
    case '.':
      glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
      break;
    case '3':
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    case 'w':
      camera.Advance(1.0f);
      camera.SetHeight(map->Interpolate(camera.GetFrom()[0], camera.GetFrom()[2]) + 2.0f);
      break;
    case 's':
      camera.Advance(-1.0f);
      camera.SetHeight(map->Interpolate(camera.GetFrom()[0], camera.GetFrom()[2]) + 2.0f);
      break;
    case 'd':
      camera.Strafe(1.0f);
      camera.SetHeight(map->Interpolate(camera.GetFrom()[0], camera.GetFrom()[2]) + 2.0f);
      break;
    case 'a':
      camera.Strafe(-1.0f);
      camera.SetHeight(map->Interpolate(camera.GetFrom()[0], camera.GetFrom()[2]) + 2.0f);
      break;
    case 'q':
      camera.Yaw(3.0f);
      break;
    case 'e':
      camera.Yaw(-3.0f);
      break;
    case '[':
      camera.Roll(-3.0f);
      break;
    case ']':
      camera.Roll(3.0f);
      break;
    case '{':
      camera.Pitch(-3.0f);
      break;
    case '}':
      camera.Pitch(3.0f);
      break;
    case 't':
      {
        light_depth_texture->Bind();
        unsigned char pixels[SIZE * SIZE * 1];
        glGetTexImage(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, pixels);

        std::ofstream out("screenshot.ppm");
        out << "P2" << std::endl;
        out << SIZE << " " << SIZE << std::endl;
        out << "255" << std::endl;
        for (int r = SIZE - 1; r >= 0; --r) {
          for (int c = 0; c < SIZE; ++c) {
            unsigned char *pixel = pixels + (r * SIZE + c) * 1;
            out << (int) pixel[0] << std::endl;
            pixel += 1;
          }
        }

        out.close();

        system("display screenshot.ppm &");
        light_depth_texture->Unbind();
      }
      break;
    default:
      BaseRenderer::OnKey(key);
      break;  
  }
}

/* ------------------------------------------------------------------------- */

void ShadowRenderer::OnMouseHover(int x, int y) {
  if (mouse_at[0] != -1) {
    int diff_x = x - mouse_at[0];
    camera.Yaw(diff_x / -2.0f);
  }
  
  if (mouse_at[1] != -1) {
    int diff_y = y - mouse_at[1];
    camera.Pitch(diff_y / 10.0f);
  }

  mouse_at[0] = x;
  mouse_at[1] = y;
}

/* ------------------------------------------------------------------------- */

void ShadowRenderer::OnMousePass(bool is_entering) {
  if (is_entering) {
    mouse_at[0] = mouse_at[1] = -1;
  }
}

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  ShadowRenderer *renderer = new ShadowRenderer();
  glut_render(renderer); 
  return 0;
}

/* ------------------------------------------------------------------------- */

// shadows
