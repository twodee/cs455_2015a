#version 120

/* uniform sampler2DShadow light_depth_texture; */
uniform sampler2D light_depth_texture;
uniform vec3 albedo = vec3(0.5, 0.2, 0.2);
uniform vec3 light_position_eye;

const float diffuse_weight = 0.7;

varying vec3 fnormal;
varying vec3 fposition_eye;
varying vec4 ftexcoord;

void main() {
  vec3 light_direction = normalize(light_position_eye - fposition_eye);
  vec3 normal = normalize(fnormal);
  float litness = max(dot(normal, light_direction), 0.0);
  float bias = 0.01 * tan(acos(litness));
  bias = clamp(bias, 0,0.01);
  vec3 a = ftexcoord.xyz / ftexcoord.w;
  float least_depth = texture2D(light_depth_texture, a.xy).r;
  float this_depth = a.z - bias;
  float shadowedness = least_depth < this_depth ? 0.5 : 1.0;

  vec3 diffuse_color = litness * albedo;
  vec3 ambient_color = albedo;
  vec3 color = diffuse_weight * diffuse_color + (1.0 - diffuse_weight) * ambient_color;

  /* shadowedness = ftexcoord.w < 1.0 ? 1.0 : shadowedness; */
  shadowedness = dot(normal, light_direction) < 0.0 ? 0.5 : shadowedness;
  gl_FragColor = shadowedness * vec4(color, 1.0);
}

// shadows
