add_definitions(-DSHADERS_DIR="${CMAKE_CURRENT_SOURCE_DIR}")

add_executable(waterbak main.cpp ../libtwodee/Heightmap.cpp ${COMMON})
target_link_libraries(waterbak ${GLUT_LIBRARIES} ${GLEW_LIBRARY} ${OPENGL_LIBRARIES})

if(WIN32)
  add_custom_command(
      TARGET waterbak POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy_if_different
      "${GLUT_DLL}"
      $<TARGET_FILE_DIR:waterbak>)
    
  add_custom_command(
      TARGET waterbak POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy_if_different
      "${GLEW_DLL}"
      $<TARGET_FILE_DIR:waterbak>)
endif(WIN32)
