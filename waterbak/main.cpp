#include <cmath>
#include <iostream>
#include <fstream>

#include "BaseRenderer.h"
#include "Camera.h"
#include "glut_utilities.h"
#include "Field2.h"
#include "Field3.h"
#include "Heightmap.h"
#include "Image.h"
#include "Matrix4.h"
#include "ObjUtilities.h"
#include "ObjUtilities.h"
#include "VertexArray.h"

using namespace td;

/* ------------------------------------------------------------------------- */

class WaterRenderer : public BaseRenderer {
  public:
    void OnDraw(float delta);
    void OnInitialize();
    void OnKey(Keys::key_t key);
    void OnSizeChanged(int width, int height);
    void OnMouseHover(int x, int y);
    void OnMousePass(bool is_entering);

  private:
    td::Heightmap *map;
    VertexAttributes *attributes[3];
    ShaderProgram *shader_programs[3];
    VertexArray *terrain;
    VertexArray *skybox;
    VertexArray *water;
    Texture *skybox_texture;
    Texture *noise_texture;
    Camera camera;
    int mouse_at[2];
    bool has_mouse;
};

/* ------------------------------------------------------------------------- */

void WaterRenderer::OnDraw(float delta) {
  BaseRenderer::OnDraw(delta);

  // Draw terrain -------------------------------------------------------------
  shader_programs[0]->Bind();
  shader_programs[0]->SetUniform("albedo", 0.3f, 1.0f, 0.3f);
  shader_programs[0]->SetUniform("modelview", camera.GetViewMatrix() * Matrix4::GetIdentity());

  terrain->Bind();
  terrain->DrawIndexed(GL_TRIANGLES);
  terrain->Unbind();

  shader_programs[0]->Unbind();

  // Draw skybox --------------------------------------------------------------
  shader_programs[1]->Bind();
  shader_programs[1]->SetUniform("modelview", camera.GetViewMatrix() * Matrix4::GetTranslate(camera.GetFrom()));
  shader_programs[1]->SetUniform("skybox_texture", *skybox_texture);

  skybox->Bind();
  skybox->DrawIndexed(GL_TRIANGLES);
  skybox->Unbind();

  shader_programs[1]->Unbind();

  // Draw water ---------------------------------------------------------------
  static float time = 0.0f;
  time += delta * 0.025f;
  if (time > 2.0f) {
    time -= 2.0f;
  }

  shader_programs[2]->Bind();
  shader_programs[2]->SetUniform("surface", *noise_texture);
  shader_programs[2]->SetUniform("skybox", *skybox_texture);
  shader_programs[2]->SetUniform("modelview", camera.GetViewMatrix() * Matrix4::GetIdentity());
  shader_programs[2]->SetUniform("model_to_world", Matrix4::GetIdentity());
  shader_programs[2]->SetUniform("time", time);
  shader_programs[2]->SetUniform("camera_world", camera.GetFrom()[0], camera.GetFrom()[1], camera.GetFrom()[2]);

  water->Bind();
  water->DrawIndexed(GL_TRIANGLES);
  water->Unbind();

  shader_programs[2]->Unbind();
}

/* ------------------------------------------------------------------------- */

void WaterRenderer::OnInitialize() {
  SetBackgroundColor(30.0f / 255, 150.0f / 255, 250.0f / 255, 1.0f);
  glLineWidth(5.0f);
  glPointSize(5.0f);

  glDepthFunc(GL_LEQUAL);
  glEnable(GL_DEPTH_TEST);

  // Terrain ------------------------------------------------------------------
  shader_programs[0] = ShaderProgram::FromFiles(SHADERS_DIR "/v.glsl", SHADERS_DIR "/f.glsl");

  int nvertices;
  Vector4 *positions;
  Vector4 *normals;
  int nfaces;
  int *faces;
  map = new Heightmap(MODELS_DIR "/island.pgm", 20.0f);
  map->ToAttributes(nvertices, positions, normals, nfaces, faces);

  attributes[0] = new VertexAttributes();
  attributes[0]->AddAttribute("position", nvertices, 4, &positions[0][0]);
  attributes[0]->AddAttribute("normal", nvertices, 4, &normals[0][0]);
  attributes[0]->AddIndices(nfaces * 3, faces);

  terrain = new VertexArray(*shader_programs[0], *attributes[0]);

  delete[] positions;
  delete[] normals;
  delete[] faces;

  // Skybox ---------------------------------------------------------------
  attributes[1] = ObjUtilities::Read(MODELS_DIR "/skybox.obj");
  shader_programs[1] = ShaderProgram::FromFiles(SHADERS_DIR "/skybox.v.glsl", SHADERS_DIR "/skybox.f.glsl");
  skybox = new VertexArray(*shader_programs[1], *attributes[1]);

#define SKYBOX "hell"
  Image *images[] = {
    new Image(MODELS_DIR "/" SKYBOX "_left.ppm"),
    new Image(MODELS_DIR "/" SKYBOX "_right.ppm"),
    new Image(MODELS_DIR "/" SKYBOX "_bottom.ppm"),
    new Image(MODELS_DIR "/" SKYBOX "_top.ppm"),
    new Image(MODELS_DIR "/" SKYBOX "_back.ppm"),
    new Image(MODELS_DIR "/" SKYBOX "_front.ppm")
  };
  skybox_texture = new Texture();
  skybox_texture->Wrap(Texture::CLAMP_TO_EDGE);
  glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  skybox_texture->Upload(images[0]->GetWidth(), images[0]->GetHeight(), images[0]->GetPixels(), images[1]->GetPixels(), images[2]->GetPixels(), images[3]->GetPixels(), images[4]->GetPixels(), images[5]->GetPixels());

  for (int i = 0; i < 6; ++i) {
    delete images[i];
  }

  // Water surface ------------------------------------------------------------
  {
    float positions[] = {
      0.0f, 9.0f, 0.0f, 1.0f,
      511.0f, 9.0f, 0.0f, 1.0f,
      0.0f, 9.0f, 511.0f, 1.0f,
      511.0f, 9.0f, 511.0f, 1.0f
    };

    float texcoords[] = {
      0.0f, 0.0f,
      1.0f, 0.0f,
      0.0f, 1.0f,
      1.0f, 1.0f
    };

    float normals[] = {
      0.0f, 0.0f, 1.0f, 0.0f,
      0.0f, 0.0f, 1.0f, 0.0f,
      0.0f, 0.0f, 1.0f, 0.0f,
      0.0f, 0.0f, 1.0f, 0.0f
    };

    int faces[] = {
      0, 1, 3,
      0, 3, 2
    };

    attributes[2] = new VertexAttributes();
    attributes[2]->AddAttribute("position", 4, 4, positions);
    attributes[2]->AddAttribute("texcoord", 4, 2, texcoords);
    attributes[2]->AddIndices(2 * 3, faces);

    shader_programs[2] = ShaderProgram::FromFiles(SHADERS_DIR "/water.v.glsl", SHADERS_DIR "/water.f.glsl");
    water = new VertexArray(*shader_programs[2], *attributes[2]);

    // TODO: Add noise texture.
    noise_texture = new Texture();
    noise_texture->Wrap(Texture::MIRRORED_REPEAT);
    noise_texture->Channels(Texture::GRAYSCALE);

    Field3 *noise = Field3::GetSmoothNoise(64, 64, 64, 5);
    noise_texture->Upload(noise->GetWidth(), noise->GetHeight(), noise->GetDepth(),  noise->GetIntensities());
    delete noise;
  }

  // --------------------------------------------------------------------------
  int midx = 276;
  int midz = 261;
  float midy = (*map)(midx, midz);

  camera.LookAt(Vector4(midx, midy + 2.0f, midz, 1.0f), 
                Vector4(midx, midy + 2.0f, midz + 10.0f, 1.0f),
                Vector4(0.0f, 1.0f, 0.0f, 0.0f));

  OpenGL::CheckError("finished oninitialize");
}

/* ------------------------------------------------------------------------- */

void WaterRenderer::OnSizeChanged(int width, int height) {
  BaseRenderer::OnSizeChanged(width, height);

  Matrix4 projection = Matrix4::GetPerspective(45.0f, GetAspectRatio(), 0.1f, 400.0f);

  for (int i = 0; i < 3; ++i) {
    shader_programs[i]->Bind();
    shader_programs[i]->SetUniform("projection", projection);
    shader_programs[i]->Unbind();  
  }

  OpenGL::CheckError("on size changed");
}

/* ------------------------------------------------------------------------- */

void WaterRenderer::OnKey(Keys::key_t key) {
  switch (key) {
    case '|':
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      break;
    case '.':
      glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
      break;
    case '3':
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    case 'w':
      camera.Advance(1.0f);
      camera.SetHeight(map->Interpolate(camera.GetFrom()[0], camera.GetFrom()[2]) + 2.0f);
      break;
    case 's':
      camera.Advance(-1.0f);
      camera.SetHeight(map->Interpolate(camera.GetFrom()[0], camera.GetFrom()[2]) + 2.0f);
      break;
    case 'd':
      camera.Strafe(1.0f);
      camera.SetHeight(map->Interpolate(camera.GetFrom()[0], camera.GetFrom()[2]) + 2.0f);
      break;
    case 'a':
      camera.Strafe(-1.0f);
      camera.SetHeight(map->Interpolate(camera.GetFrom()[0], camera.GetFrom()[2]) + 2.0f);
      break;
    case 'q':
      camera.Yaw(3.0f);
      break;
    case 'e':
      camera.Yaw(-3.0f);
      break;
    case '[':
      camera.Roll(-3.0f);
      break;
    case ']':
      camera.Roll(3.0f);
      break;
    case '{':
      camera.Pitch(-3.0f);
      break;
    case '}':
      camera.Pitch(3.0f);
      break;
    default:
      BaseRenderer::OnKey(key);
      break;  
  }
}

/* ------------------------------------------------------------------------- */

void WaterRenderer::OnMouseHover(int x, int y) {
  if (mouse_at[0] != -1) {
    int diff_x = x - mouse_at[0];
    camera.Yaw(diff_x / -2.0f);
  }
  
  if (mouse_at[1] != -1) {
    int diff_y = y - mouse_at[1];
    camera.Pitch(diff_y / 10.0f);
  }

  mouse_at[0] = x;
  mouse_at[1] = y;
}

/* ------------------------------------------------------------------------- */

void WaterRenderer::OnMousePass(bool is_entering) {
  if (is_entering) {
    mouse_at[0] = mouse_at[1] = -1;
  }
}

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  WaterRenderer *renderer = new WaterRenderer();
  glut_render(renderer); 
  return 0;
}

/* ------------------------------------------------------------------------- */

// water
