#version 120

uniform sampler3D surface;
uniform samplerCube skybox;
uniform vec3 albedo = vec3(0.3, 0.3, 0.5);
uniform vec3 camera_world;

varying vec2 ftexcoord;
varying vec3 fposition_world;

uniform float time = 0.5;

void main() {
  float here = texture3D(surface, vec3(ftexcoord, time)).r;
  float right = texture3D(surface, vec3(ftexcoord, time) + vec3(0.1, 0.0, 0.0)).r;
  float up = texture3D(surface, vec3(ftexcoord, time) + vec3(0.0, 0.1, 0.0)).r;

  vec3 tangentRight = vec3(1.0, right - here, 0.0);
  vec3 tangentUp = vec3(0.0, up - here, 1.0);
  vec3 fnormal_world = normalize(cross(tangentRight, tangentUp));

  vec3 incident = normalize(fposition_world - camera_world);

  vec3 r = reflect(incident, fnormal_world);
  /* vec3 r = reflect(incident, vec3(0.0, 1.0, 0.0)); */
  vec3 env = textureCube(skybox, r).rgb;
  vec3 rgb = env * albedo;

  gl_FragColor = vec4(rgb, 1.0);
}

// water
