#version 120

uniform mat4 projection;
uniform mat4 modelview;
uniform mat4 model_to_world;

attribute vec3 position;
attribute vec3 normal;
attribute vec2 texcoord;

varying vec3 fposition_world;
varying vec2 ftexcoord;

void main() {
  vec4 position_eye = modelview * vec4(position, 1.0);
  gl_Position = projection * position_eye;

  fposition_world = (model_to_world * vec4(position, 1.0)).xyz;
  ftexcoord = texcoord;
}

// water
