#include <cmath>
#include <iostream>
#include <fstream>

#include "BaseRenderer.h"
#include "Camera.h"
#include "glut_utilities.h"
#include "Image.h"
#include "Matrix4.h"
#include "ObjUtilities.h"
#include "VertexArray.h"

using namespace td;

/* ------------------------------------------------------------------------- */

class BatSignalRenderer : public BaseRenderer {
  public:
    void OnDraw(float delta);
    void OnInitialize();
    void OnKey(Keys::key_t key);
    void OnSizeChanged(int width, int height);
    void OnMouseHover(int x, int y);
    void OnMousePass(bool is_entering);

  private:
    VertexAttributes *attributes[2];
    ShaderProgram *shader_programs[2];
    VertexArray *box;
    VertexArray *cone;
    Texture *bat_signal_texture;
    Camera camera;
    Camera light_camera;
    int mouse_at[2];
};

/* ------------------------------------------------------------------------- */

void BatSignalRenderer::OnDraw(float delta) {
  BaseRenderer::OnDraw(delta);

  // --------------------------------------------------------------------------
  // TODO add a transform for turning model coordinate's into texture
  // coordinates
  shader_programs[0]->Bind();
  shader_programs[0]->SetUniform("modelview", camera.GetViewMatrix());

  box->Bind();
  box->DrawIndexed(GL_TRIANGLES);
  box->Unbind();

  shader_programs[0]->Unbind();

  // --------------------------------------------------------------------------
  // TODO fix cone's orientation

  shader_programs[1]->Bind();
  shader_programs[1]->SetUniform("modelview", camera.GetViewMatrix() * Matrix4::GetIdentity());

  cone->Bind();
  cone->DrawIndexed(GL_TRIANGLES);
  cone->Unbind();

  shader_programs[1]->Unbind();
}

/* ------------------------------------------------------------------------- */

void BatSignalRenderer::OnInitialize() {
  SetBackgroundColor(0.2f, 0.0f, 0.4f, 1.0f);
  glLineWidth(5.0f);
  glPointSize(5.0f);

  glDepthFunc(GL_LEQUAL);
  glEnable(GL_DEPTH_TEST);

  attributes[0] = ObjUtilities::Read(MODELS_DIR "/inward_box.obj");
  shader_programs[0] = ShaderProgram::FromFiles(SHADERS_DIR "/v.glsl",
                                                SHADERS_DIR "/f.glsl");
  box = new VertexArray(*shader_programs[0], *attributes[0]);

  attributes[1] = ObjUtilities::Read(MODELS_DIR "/cone.obj");
  shader_programs[1] = ShaderProgram::FromFiles(SHADERS_DIR "/spot.v.glsl",
                                                SHADERS_DIR "/spot.f.glsl");
  cone = new VertexArray(*shader_programs[1], *attributes[1]);

  // --------------------------------------------------------------------------
  // TODO add image

  // --------------------------------------------------------------------------
  // TODO add texture

  // --------------------------------------------------------------------------
  // TODO initialize light camera
 
  camera.LookAt(Vector4(0.0f, 0.0f, -1.0f), 
                Vector4(0.0f, 0.0f, -10.0f),
                Vector4(0.0f, 1.0f, 0.0f, 0.0f));

  OpenGL::CheckError("finished oninitialize");
}

/* ------------------------------------------------------------------------- */

void BatSignalRenderer::OnSizeChanged(int width, int height) {
  BaseRenderer::OnSizeChanged(width, height);

  Matrix4 projection = Matrix4::GetPerspective(45.0f, GetAspectRatio(), 0.1f, 400.0f);

  for (int i = 0; i < 2; ++i) {
    shader_programs[i]->Bind();
    shader_programs[i]->SetUniform("projection", projection);
    shader_programs[i]->Unbind();  
  }

  OpenGL::CheckError("on size changed");
}

/* ------------------------------------------------------------------------- */

void BatSignalRenderer::OnKey(Keys::key_t key) {
  switch (key) {
    // TODO add light camera manipulation
    case Keys::LEFT:
      break;
    case Keys::RIGHT:
      break;
    case Keys::UP:
      break;
    case Keys::DOWN:
      break;
    case '|':
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      break;
    case '.':
      glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
      break;
    case '3':
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    case 'w':
      camera.Advance(0.1f);
      break;
    case 's':
      camera.Advance(-0.1f);
      break;
    case 'd':
      camera.Strafe(0.1f);
      break;
    case 'a':
      camera.Strafe(-0.1f);
      break;
    case 'q':
      camera.Yaw(3.0f);
      break;
    case 'e':
      camera.Yaw(-3.0f);
      break;
    case '[':
      camera.Roll(-3.0f);
      break;
    case ']':
      camera.Roll(3.0f);
      break;
    case '{':
      camera.Pitch(-3.0f);
      break;
    case '}':
      camera.Pitch(3.0f);
      break;
    default:
      BaseRenderer::OnKey(key);
      break;  
  }
}

/* ------------------------------------------------------------------------- */

void BatSignalRenderer::OnMouseHover(int x, int y) {
  if (mouse_at[0] != -1) {
    int diff_x = x - mouse_at[0];
    camera.Yaw(diff_x / -2.0f);
  }
  
  if (mouse_at[1] != -1) {
    int diff_y = y - mouse_at[1];
    camera.Pitch(diff_y / 10.0f);
  }

  mouse_at[0] = x;
  mouse_at[1] = y;
}

/* ------------------------------------------------------------------------- */

void BatSignalRenderer::OnMousePass(bool is_entering) {
  if (is_entering) {
    mouse_at[0] = mouse_at[1] = -1;
  }
}

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  BatSignalRenderer *renderer = new BatSignalRenderer();
  glut_render(renderer); 
  return 0;
}

/* ------------------------------------------------------------------------- */

// batsignal
