#version 120

uniform vec3 albedo = vec3(0.5, 0.2, 0.2);

const float diffuse_weight = 0.7;
const vec3 light_position = vec3(0.0, 0.0, 0.0);

varying vec3 fnormal;
varying vec3 fposition_eye;

void main() {
  vec3 light_direction = normalize(light_position - fposition_eye);
  vec3 normal = normalize(fnormal);
  float litness = max(dot(normal, light_direction), 0.0);
  vec3 diffuse_color = litness * albedo;
  vec3 ambient_color = albedo;
  vec3 color = diffuse_weight * diffuse_color + (1.0 - diffuse_weight) * ambient_color;
  gl_FragColor = vec4(color, 1.0);
}

// batsignal
