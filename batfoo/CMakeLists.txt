add_definitions(-DSHADERS_DIR="${CMAKE_CURRENT_SOURCE_DIR}")

add_executable(batsignal main.cpp ../libtwodee/Heightmap.cpp ${COMMON})
target_link_libraries(batsignal ${GLUT_LIBRARIES} ${GLEW_LIBRARY} ${OPENGL_LIBRARIES})

if(WIN32)
  add_custom_command(
      TARGET batsignal POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy_if_different
      "${GLUT_DLL}"
      $<TARGET_FILE_DIR:batsignal>)
    
  add_custom_command(
      TARGET batsignal POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy_if_different
      "${GLEW_DLL}"
      $<TARGET_FILE_DIR:batsignal>)
endif(WIN32)
