#version 120

uniform vec3 albedo;
uniform vec3 light_position_eye;

varying vec3 fnormal_eye;
varying vec3 fposition_eye;

const vec3 diffuse_color = vec3(1.0, 1.0, 1.0);
const vec3 specular_color = vec3(1.0, 1.0, 1.0);
const float diffuse_weight = 0.8;
const float shininess = 20;

void main() {
  vec3 normal_eye = normalize(fnormal_eye);

  // Find vector from fragment to light.
  vec3 light_direction_eye = normalize(light_position_eye - fposition_eye);
  vec3 eye_direction_eye = normalize(-fposition_eye);
  vec3 halfway = normalize(light_direction_eye + eye_direction_eye);

  // Diffuse calculation.
  float n_dot_l = max(dot(normal_eye, light_direction_eye), 0.0);
  vec3 diffuse = n_dot_l * albedo * diffuse_color * diffuse_weight;

  // Ambient calculation.
  vec3 ambient = (1.0 - diffuse_weight) * diffuse_color;

  // Specular calculation.
  /* float n_dot_h = max(dot(normal_eye, halfway), 0.0) * (n_dot_l > 0 ? 1 : 0); */
  float n_dot_h = max(dot(normal_eye, halfway), 0.0) * step(0.0, n_dot_l);
  vec3 specular = pow(n_dot_h, shininess) * specular_color;

  gl_FragColor = vec4(ambient + diffuse + specular, 1.0);
}

// specular
