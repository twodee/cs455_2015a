#include <cmath>
#include <iostream>
#include <fstream>

#include "BaseRenderer.h"
#include "Camera.h"
#include "glut_utilities.h"
#include "Image.h"
#include "Matrix4.h"
#include "ObjUtilities.h"
#include "ObjUtilities.h"
#include "VertexArray.h"

using namespace td;

/* ------------------------------------------------------------------------- */

class SpecularRenderer : public BaseRenderer {
  public:
    void OnDraw(float delta);
    void OnInitialize();
    void OnKey(Keys::key_t key);
    void OnSizeChanged(int width, int height);

  private:
    VertexAttributes *attributes[3];
    ShaderProgram *shader_program[2];
    VertexArray *objects[3];
    Texture *texture;
    Camera camera;
    Matrix4 light_model;
};

/* ------------------------------------------------------------------------- */

void SpecularRenderer::OnDraw(float delta) {
  BaseRenderer::OnDraw(delta);

  // Update light's rotation.
  static float angle = 0.0f;
  angle += delta * 25.0f;
  if (angle > 360.0f) {
    angle -= 360.0f;
  }

  Matrix4 light_xform = camera.GetViewMatrix() * Matrix4::GetRotate(angle, Vector4(0.0f, 1.0f, 0.0f, 0.0f)) * Matrix4::GetTranslate(Vector4(10.0f, 10.0f, -20.0f, 0.0f));
  Vector4 light_position_eye = light_xform * Vector4(0.0f, 0.0f, 0.0f, 1.0f);

  // First draw the lit stuff.
  shader_program[0]->Bind();
  shader_program[0]->SetUniform("light_position_eye", light_position_eye[0], light_position_eye[1], light_position_eye[2]);

  // Floor
  shader_program[0]->SetUniform("albedo", 0.3f, 1.0f, 0.3f);
  shader_program[0]->SetUniform("modelview", camera.GetViewMatrix() * Matrix4::GetIdentity());
  objects[0]->Bind();
  objects[0]->DrawIndexed(GL_TRIANGLES);
  objects[0]->Unbind();

  // Monkey
  shader_program[0]->SetUniform("albedo", 1.0f, 0.5f, 1.0f);
  shader_program[0]->SetUniform("modelview", camera.GetViewMatrix() * Matrix4::GetTranslate(Vector4(0.0f, 4.0f, 0.0f, 0.0f)) * Matrix4::GetScale(Vector4(5.0f, 5.0f, 5.0f, 0.0f)));
  objects[1]->Bind();
  objects[1]->DrawIndexed(GL_TRIANGLES);
  objects[1]->Unbind();

  shader_program[0]->Unbind();

  // Now draw the unlit stuff.
  shader_program[1]->Bind();

  // Light
  shader_program[1]->SetUniform("albedo", 1.0f, 1.0f, 0.0f);
  shader_program[1]->SetUniform("modelview", light_xform * Matrix4::GetScale(Vector4(3.0f, 3.0f, 3.0f, 0.0f)));
  objects[2]->Bind();
  objects[2]->DrawIndexed(GL_TRIANGLES);
  objects[2]->Unbind();

  shader_program[1]->Unbind();
}

/* ------------------------------------------------------------------------- */

void SpecularRenderer::OnInitialize() {
  SetBackgroundColor(30.0f / 255, 150.0f / 255, 250.0f / 255, 1.0f);

  glEnable(GL_DEPTH_TEST);
  shader_program[0] = ShaderProgram::FromFiles(SHADERS_DIR "/v.glsl", SHADERS_DIR "/f.glsl");
  shader_program[1] = ShaderProgram::FromFiles(SHADERS_DIR "/v_unlit.glsl", SHADERS_DIR "/f_unlit.glsl");

  // Floor
  {
    int nvertices = 4;
    int nfaces = 2;
    int faces[] = {
      0, 1, 3,
      0, 3, 2
    };
    float positions[] = {
      -100.0f, 0.0f, 100.0f,
       100.0f, 0.0f, 100.0f,
      -100.0f, 0.0f, -100.0f,
       100.0f, 0.0f, -100.0f
    };
    float texcoords[] = {
      0.0f, 0.0f,
      1.0f, 0.0f,
      0.0f, 1.0f,
      1.0f, 1.0f
    };
    float normals[] = {
      0.0f, 1.0f, 0.0f, 0.0f,
      0.0f, 1.0f, 0.0f, 0.0f,
      0.0f, 1.0f, 0.0f, 0.0f,
      0.0f, 1.0f, 0.0f, 0.0f
    };

    attributes[0] = new VertexAttributes();
    attributes[0]->AddAttribute("position", nvertices, 3, positions);
    attributes[0]->AddAttribute("normal", nvertices, 4, normals);
    attributes[0]->AddIndices(nfaces * 3, faces);

    objects[0] = new VertexArray(*shader_program[0], *attributes[0]);
  }

  // Monkey
  {
    int nvertices;
    float *positions;
    float *texcoords;
    Vector4 *normals;
    int nfaces;
    int *faces;
    ObjUtilities::Read(MODELS_DIR "/suzanne.obj", nvertices, positions, texcoords, normals, nfaces, faces);

    attributes[1] = new VertexAttributes();
    attributes[1]->AddAttribute("position", nvertices, 3, positions);
    attributes[1]->AddAttribute("normal", nvertices, 4, &normals[0][0]);
    attributes[1]->AddIndices(nfaces * 3, faces);

    objects[1] = new VertexArray(*shader_program[0], *attributes[1]);

    delete[] positions;
    delete[] texcoords;
    delete[] normals;
    delete[] faces;
  }

  // Lightball
  {
    int nvertices;
    float *positions;
    float *texcoords;
    Vector4 *normals;
    int nfaces;
    int *faces;
    ObjUtilities::Read(MODELS_DIR "/unit_sphere.obj", nvertices, positions, texcoords, normals, nfaces, faces);

    attributes[2] = new VertexAttributes();
    attributes[2]->AddAttribute("position", nvertices, 3, positions);
    attributes[2]->AddAttribute("normal", nvertices, 4, &normals[0][0]);
    attributes[2]->AddIndices(nfaces * 3, faces);

    objects[2] = new VertexArray(*shader_program[1], *attributes[2]);

    delete[] positions;
    delete[] texcoords;
    delete[] normals;
    delete[] faces;
  }

  camera.LookAt(Vector4(0.0f, 2.0f, 10.0f, 0.0f), 
                Vector4(0.0f, 2.0f, 0.0f, 0.0f),
                Vector4(0.0f, 1.0f, 0.0f, 0.0f));

  OpenGL::CheckError("finished oninitialize");
}

/* ------------------------------------------------------------------------- */

void SpecularRenderer::OnSizeChanged(int width, int height) {
  BaseRenderer::OnSizeChanged(width, height);

  Matrix4 projection = Matrix4::GetPerspective(45.0f, GetAspectRatio(), 0.1f, 100.0f);

  shader_program[0]->Bind();
  shader_program[0]->SetUniform("projection", projection);
  shader_program[0]->Unbind();  
  shader_program[1]->Bind();
  shader_program[1]->SetUniform("projection", projection);
  shader_program[1]->Unbind();  
}

/* ------------------------------------------------------------------------- */

void SpecularRenderer::OnKey(Keys::key_t key) {
  switch (key) {
    case '|':
      std::cout << "pipe" << std::endl;
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      OpenGL::CheckError("line bad");
      break;
    case '.':
      glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
      break;
    case '3':
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    case 'w':
      camera.Advance(3.0f);
      break;
    case 's':
      camera.Advance(-3.0f);
      break;
    case 'd':
      camera.Strafe(3.0f);
      break;
    case 'a':
      camera.Strafe(-3.0f);
      break;
    case 'q':
      camera.Yaw(3.0f);
      break;
    case 'e':
      camera.Yaw(-3.0f);
      break;
    case '[':
      camera.Roll(-3.0f);
      break;
    case ']':
      camera.Roll(3.0f);
      break;
    case '{':
      camera.Pitch(-3.0f);
      break;
    case '}':
      camera.Pitch(3.0f);
      break;
    default:
      BaseRenderer::OnKey(key);
      break;  
  }
}

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  SpecularRenderer *renderer = new SpecularRenderer();
  glut_render(renderer); 
  return 0;
}

/* ------------------------------------------------------------------------- */

// specular
