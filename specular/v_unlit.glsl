#version 120

attribute vec3 position;

uniform mat4 projection;
uniform mat4 modelview;

void main() {
  vec4 position_eye = modelview * vec4(position, 1.0);
  gl_Position = projection * position_eye;
}

// specular
