#version 120

uniform vec3 albedo;

void main() {
  gl_FragColor = vec4(albedo, 1.0);
}

// specular
