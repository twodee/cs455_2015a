#include <cmath>
#include <iostream>
#include <fstream>

#include "BaseRenderer.h"
#include "Camera.h"
#include "glut_utilities.h"
#include "Heightmap.h"
#include "Image.h"
#include "Matrix4.h"
#include "ObjUtilities.h"
#include "ObjUtilities.h"
#include "VertexArray.h"

using namespace td;

/* ------------------------------------------------------------------------- */

class HeightmapRenderer : public BaseRenderer {
  public:
    void OnDraw(float delta);
    void OnInitialize();
    void OnKey(Keys::key_t key);
    void OnSizeChanged(int width, int height);

  private:
    td::Heightmap *map;
    VertexAttributes *attributes;
    ShaderProgram *shader_program;
    VertexArray *terrain;
    Texture *texture;
    Camera camera;
    Matrix4 light_model;
};

/* ------------------------------------------------------------------------- */

void HeightmapRenderer::OnDraw(float delta) {
  BaseRenderer::OnDraw(delta);

  shader_program->Bind();

  // Floor
  shader_program->SetUniform("albedo", 0.3f, 1.0f, 0.3f);
  shader_program->SetUniform("modelview", camera.GetViewMatrix() * Matrix4::GetIdentity());

  terrain->Bind();
  terrain->DrawIndexed(GL_TRIANGLES);
  terrain->Unbind();

  shader_program->Unbind();
}

/* ------------------------------------------------------------------------- */

void HeightmapRenderer::OnInitialize() {
  SetBackgroundColor(30.0f / 255, 150.0f / 255, 250.0f / 255, 1.0f);
  glLineWidth(5.0f);
  glPointSize(5.0f);

  glEnable(GL_DEPTH_TEST);
  shader_program = ShaderProgram::FromFiles(SHADERS_DIR "/v.glsl", SHADERS_DIR "/f.glsl");

  // Heightmap
  int nvertices;
  Vector4 *positions;
  Vector4 *normals;
  int nfaces;
  int *faces;
  map = new Heightmap(MODELS_DIR "/noise.pgm", 10.0f);
  map->ToAttributes(nvertices, positions, normals, nfaces, faces);

  attributes = new VertexAttributes();
  attributes->AddAttribute("position", nvertices, 4, &positions[0][0]);
  attributes->AddAttribute("normal", nvertices, 4, &normals[0][0]);
  attributes->AddIndices(nfaces * 3, faces);

  terrain = new VertexArray(*shader_program, *attributes);

  delete[] positions;
  delete[] normals;
  delete[] faces;

  int midx = map->GetWidth() / 2;
  int midz = map->GetDepth() / 2;
  float midy = (*map)(midx, midz);

  camera.LookAt(Vector4(midx, midy + 2.0f, midz, 0.0f), 
                Vector4(midx, midy + 2.0f, midz - 10.0f, 0.0f),
                Vector4(0.0f, 1.0f, 0.0f, 0.0f));

  OpenGL::CheckError("finished oninitialize");
}

/* ------------------------------------------------------------------------- */

void HeightmapRenderer::OnSizeChanged(int width, int height) {
  BaseRenderer::OnSizeChanged(width, height);

  Matrix4 projection = Matrix4::GetPerspective(45.0f, GetAspectRatio(), 0.1f, 100.0f);

  shader_program->Bind();
  shader_program->SetUniform("projection", projection);
  shader_program->Unbind();  
}

/* ------------------------------------------------------------------------- */

void HeightmapRenderer::OnKey(Keys::key_t key) {
  switch (key) {
    case '|':
      std::cout << "pipe" << std::endl;
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      OpenGL::CheckError("line bad");
      break;
    case '.':
      glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
      break;
    case '3':
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    case 'w':
      camera.Advance(1.0f);
      camera.SetHeight(map->Interpolate(camera.GetFrom()[0], camera.GetFrom()[2]) + 2.0f);
      break;
    case 's':
      camera.Advance(-1.0f);
      camera.SetHeight(map->Interpolate(camera.GetFrom()[0], camera.GetFrom()[2]) + 2.0f);
      break;
    case 'd':
      camera.Strafe(1.0f);
      camera.SetHeight(map->Interpolate(camera.GetFrom()[0], camera.GetFrom()[2]) + 2.0f);
      break;
    case 'a':
      camera.Strafe(-1.0f);
      camera.SetHeight(map->Interpolate(camera.GetFrom()[0], camera.GetFrom()[2]) + 2.0f);
      break;
    case 'q':
      camera.Yaw(3.0f);
      break;
    case 'e':
      camera.Yaw(-3.0f);
      break;
    case '[':
      camera.Roll(-3.0f);
      break;
    case ']':
      camera.Roll(3.0f);
      break;
    case '{':
      camera.Pitch(-3.0f);
      break;
    case '}':
      camera.Pitch(3.0f);
      break;
    default:
      BaseRenderer::OnKey(key);
      break;  
  }
}

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  HeightmapRenderer *renderer = new HeightmapRenderer();
  glut_render(renderer); 
  return 0;
}

/* ------------------------------------------------------------------------- */

// heightmap
