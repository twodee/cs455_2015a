#version 120

attribute vec3 position;
attribute vec2 texcoords;

uniform mat4 projection;
uniform mat4 modelview;

varying vec2 ftexcoords;

void main() {
  gl_Position = projection * modelview * vec4(position, 1.0);
  ftexcoords = texcoords;  
}

// first_person
