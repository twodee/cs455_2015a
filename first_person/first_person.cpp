#include <cmath>
#include <iostream>
#include <fstream>

#include "BaseRenderer.h"
#include "Camera.h"
#include "glut_utilities.h"
#include "Image.h"
#include "Matrix4.h"
#include "ObjUtilities.h"
#include "ObjUtilities.h"
#include "VertexArray.h"

using namespace td;

/* ------------------------------------------------------------------------- */

class FirstPersonRenderer : public BaseRenderer {
  public:
    void OnDraw(float delta);
    void OnInitialize();
    void OnKey(Keys::key_t key);
    void OnSizeChanged(int width, int height);

  private:
    VertexAttributes *plane_attributes;
    ShaderProgram *shader_program;
    VertexArray *plane;
    Texture *texture;
    Camera camera;
};

/* ------------------------------------------------------------------------- */

void FirstPersonRenderer::OnDraw(float delta) {
  BaseRenderer::OnDraw(delta);

  shader_program->Bind();  
  shader_program->SetUniform("texture", *texture);
  shader_program->SetUniform("modelview", camera.GetViewMatrix());
  plane->Bind();
  plane->DrawIndexed(GL_TRIANGLES);
  plane->Unbind();
  shader_program->Unbind();
}

/* ------------------------------------------------------------------------- */

void FirstPersonRenderer::OnInitialize() {
  SetBackgroundColor(30.0f / 255, 150.0f / 255, 250.0f / 255, 1.0f);   

  glEnable(GL_DEPTH_TEST);

  int nvertices = 4;
  int nfaces = 2;
  int faces[] = {
    0, 1, 3,
    0, 3, 2
  };
  float positions[] = {
    -100.0f, -3.0f, 100.0f,
     100.0f, -3.0f, 100.0f,
    -100.0f, -3.0f, -100.0f,
     100.0f, -3.0f, -100.0f
  };
  float texcoords[] = {
    0.0f, 0.0f,
    1.0f, 0.0f,
    0.0f, 1.0f,
    1.0f, 1.0f
  };

  Image image(MODELS_DIR "/checkerboard.ppm");

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  texture = new Texture(0);
  texture->Wrap(Texture::CLAMP_TO_EDGE);
  texture->Magnify(Texture::MAGNIFY_NEAREST);
  texture->Upload(image.GetWidth(), image.GetHeight(), image.GetPixels());

  // Ship positions off to GPU.
  plane_attributes = new VertexAttributes();
  plane_attributes->AddAttribute("position", nvertices, 3, positions);
  plane_attributes->AddAttribute("texcoords", nvertices, 2, texcoords);
  plane_attributes->AddIndices(nfaces * 3, faces);

  shader_program = ShaderProgram::FromFiles(SHADERS_DIR "/v.glsl", SHADERS_DIR "/f.glsl");

  plane = new VertexArray(*shader_program, *plane_attributes);

  camera.LookAt(Vector4(0.0f, 2.0f, 0.0f, 0.0f), 
                Vector4(0.0f, 2.0f, -2.0f, 0.0f),
                Vector4(0.0f, 1.0f, 0.0f, 0.0f));

  OpenGL::CheckError("finished oninitialize");
}

/* ------------------------------------------------------------------------- */

void FirstPersonRenderer::OnSizeChanged(int width, int height) {
  BaseRenderer::OnSizeChanged(width, height);

  Matrix4 projection = Matrix4::GetPerspective(45.0f, GetAspectRatio(), 0.1f, 100.0f);

  shader_program->Bind();
  shader_program->SetUniform("projection", projection);
  shader_program->Unbind();  
}

/* ------------------------------------------------------------------------- */

void FirstPersonRenderer::OnKey(Keys::key_t key) {
  switch (key) {
    case 'w':
      camera.Advance(3.0f);
      break;
    case 's':
      camera.Advance(-3.0f);
      break;
    case '[':
      camera.Roll(-3.0f);
      break;
    case ']':
      camera.Roll(3.0f);
      break;
    default:
      BaseRenderer::OnKey(key);
      break;  
  }
}

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  FirstPersonRenderer *renderer = new FirstPersonRenderer();
  glut_render(renderer); 
  return 0;
}

/* ------------------------------------------------------------------------- */

// first_person
