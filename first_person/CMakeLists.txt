add_definitions(-DSHADERS_DIR="${CMAKE_CURRENT_SOURCE_DIR}")
add_executable(first_person first_person.cpp ${COMMON})
target_link_libraries(first_person ${GLUT_LIBRARIES} ${GLEW_LIBRARY} ${OPENGL_LIBRARIES})

if(WIN32)
  add_custom_command(
      TARGET first_person POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy_if_different
      "${GLUT_DLL}"
      $<TARGET_FILE_DIR:first_person>)
    
  add_custom_command(
      TARGET first_person POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy_if_different
      "${GLEW_DLL}"
      $<TARGET_FILE_DIR:first_person>)
endif(WIN32)
