#version 120

uniform sampler2D tex;

varying vec2 ftexcoords;

void main() {
  vec3 albedo = texture2D(tex, ftexcoords).xyz;
  gl_FragColor = vec4(albedo, 1.0);

}

// ninth
