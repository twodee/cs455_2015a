#include <iostream>

#include "glut_utilities.h"
#include "VertexArray.h"

using namespace td;

/* ------------------------------------------------------------------------- */

class FirstRenderer : public BaseRenderer {
  public:
    void OnInitialize();
    void OnDraw(float delta);

  private:
    VertexAttributes *rectangle_attributes;
    VertexArray *rectangle_array;
    ShaderProgram *shader_program;
};

/* ------------------------------------------------------------------------- */

void FirstRenderer::OnInitialize() {
  float positions[] = {
    -0.5f, -0.5f,
     0.5f, -0.5f,
    -0.5f,  0.5f
  }; 
  rectangle_attributes = new VertexAttributes();
  rectangle_attributes->AddAttribute("position", 3, 2, positions);

  shader_program = ShaderProgram::FromFiles(SHADERS_DIR "/white.vert.glsl", SHADERS_DIR "/white.frag.glsl");

  rectangle_array = new VertexArray(*shader_program, *rectangle_attributes);
}

/* ------------------------------------------------------------------------- */

void FirstRenderer::OnDraw(float delta) {
  BaseRenderer::OnDraw(delta);
  shader_program->Bind();  
  rectangle_array->Bind();
  rectangle_array->DrawSequence(GL_TRIANGLES);
  rectangle_array->Unbind();
  shader_program->Unbind();  
}

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  FirstRenderer *renderer = new FirstRenderer();
  glut_render(renderer);
  return 0;
}
