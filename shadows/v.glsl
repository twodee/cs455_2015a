#version 120

attribute vec3 position;
attribute vec4 normal;

uniform mat4 projection;
uniform mat4 modelview;
uniform mat4 object_to_tex;

varying vec3 fnormal;
varying vec3 fposition_eye;
varying vec4 ftexcoord;

void main() {
  vec4 position_eye = modelview * vec4(position, 1.0);
  gl_Position = projection * position_eye;
  fposition_eye = position_eye.xyz;
  fnormal = (modelview * normal).xyz;
  ftexcoord = object_to_tex * vec4(position, 1.0);
}

// shadows
