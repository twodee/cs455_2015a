#version 120

uniform vec3 albedo = vec3(0.5, 0.2, 0.2);
uniform vec3 light_position_eye;

varying vec3 fnormal;
varying vec3 fposition_eye;
varying vec4 ftexcoord;

void main() {
  vec3 light_direction = normalize(light_position_eye - fposition_eye);
  vec3 normal = normalize(fnormal);
  float litness = max(dot(normal, light_direction), 0.0);

  // TODO: Compare depths.

  vec3 diffuse_color = litness * albedo;
  vec3 color = diffuse_color;

  gl_FragColor = vec4(color, 1.0);
}

// shadows
