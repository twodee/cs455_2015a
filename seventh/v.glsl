#version 120

attribute vec3 position;
attribute vec4 normal;

uniform mat4 projection;
uniform mat4 xform;

varying vec3 fnormal;

void main() {
  gl_Position = projection * xform * vec4(position, 1.0);
  fnormal = (xform * normal).xyz;
}

// seventh
