#include <cmath>
#include <iostream>
#include <fstream>

#include "BaseRenderer.h"
#include "glut_utilities.h"
#include "Matrix4.h"
#include "VertexArray.h"

using namespace td;

/* ------------------------------------------------------------------------- */

void read(const std::string& path,
          int &npositions,
          float *&positions,
          Vector4 *&normals,
          int &nfaces,
          int *&faces) {
  std::ifstream in(path.c_str()); 
 
  std::string line;
  std::string token; 

  npositions = 0;
  nfaces = 0;

  while (in >> token) {
    if (token == "v") {
      ++npositions;
    } else if (token == "f") {
      ++nfaces;
    }
    getline(in, line);
  }

  std::cout << "npositions: " << npositions << std::endl;
  std::cout << "nfaces: " << nfaces << std::endl;

  positions = new float[npositions * 3];
  normals = new Vector4[npositions];
  faces = new int[nfaces * 3];

  in.clear();
  in.seekg(0, std::ios_base::beg);

  float *position = positions;
  int i = 0;
  int *face = faces;
  while (in >> token) {
    if (token == "v") {
      in >> position[0] >> position[1] >> position[2]; 
      normals[i++] = Vector4(0.0f);
      position += 3;
    } else if (token == "f") {
      in >> face[0] >> face[1] >> face[2]; 
      face[0] -= 1;
      face[1] -= 1;
      face[2] -= 1;

      float *a = positions + face[0] * 3;
      float *b = positions + face[1] * 3;
      float *c = positions + face[2] * 3;
      Vector4 a2b(b[0] - a[0], b[1] - a[1], b[2] - a[2], 0.0f);
      Vector4 a2c(c[0] - a[0], c[1] - a[1], c[2] - a[2], 0.0f);
      Vector4 face_normal = a2b.Cross(a2c);
      face_normal.Normalize();

      normals[face[0]] += face_normal;
      normals[face[1]] += face_normal;
      normals[face[2]] += face_normal;

      face += 3;
    }
    getline(in, line);
  }

  for (int i = 0; i < npositions; ++i) {
    normals[i].Normalize();
  }

  in.close();
}

/* ------------------------------------------------------------------------- */

class FifthRenderer : public BaseRenderer {
  public:
    void OnDraw(float delta);
    void OnInitialize();
    void OnKey(Keys::key_t key);
    void OnLeftMouseDown(int x, int y);
    void OnLeftMouseDragged(int x, int y);
    void OnLeftMouseUp(int x, int y);

  private:
    Vector4 ProjectOntoTrackball(int x_pixel, int y_pixel) const;

    VertexAttributes *plane_attributes;
    ShaderProgram *shader_program;
    VertexArray *plane;

    Vector4 fore;
    Matrix4 xform;
};

/* ------------------------------------------------------------------------- */

Vector4 FifthRenderer::ProjectOntoTrackball(int x_pixel, int y_pixel) const {
  Vector4 v_pixel = Vector4(x_pixel, y_pixel, 0.0f, 1.0f); 
  Vector4 v_ball =
    Matrix4::GetTranslate(Vector4(-1.0f, -1.0f, 0.0f)) *
    Matrix4::GetScale(Vector4(2.0f / GetWidth(), 2.0f / GetHeight(), 1.0f)) *
    v_pixel;

  float z_squared = 1.0f - v_ball[0] * v_ball[0] - v_ball[1] * v_ball[1];
  if (z_squared < 0.0f) {
    z_squared = 0.0f;
  }

  v_ball[2] = sqrtf(z_squared);
  v_ball.Normalize();

  return v_ball;
}

/* ------------------------------------------------------------------------- */

void FifthRenderer::OnLeftMouseDown(int x, int y) {
  fore = ProjectOntoTrackball(x, y);
}

/* ------------------------------------------------------------------------- */

void FifthRenderer::OnLeftMouseDragged(int x, int y) {
  Vector4 aft = ProjectOntoTrackball(x, y);

  float theta = acos(fore.Dot(aft));
  if (fabs(theta) > 1.0e-6f) {
    Vector4 axis = fore.Cross(aft);
    axis.Normalize();

    Matrix4 rotation = Matrix4::GetRotate(theta * 180.0f / 3.14159265358979323846264f, axis);

    shader_program->Bind();  
    shader_program->SetUniform("xform", rotation * xform);
    shader_program->Unbind();  
  }
}

/* ------------------------------------------------------------------------- */

void FifthRenderer::OnLeftMouseUp(int x, int y) {
  Vector4 aft = ProjectOntoTrackball(x, y);

  float theta = acos(fore.Dot(aft));
  if (fabs(theta) > 1.0e-6f) {
    Vector4 axis = fore.Cross(aft);
    axis.Normalize();

    Matrix4 rotation = Matrix4::GetRotate(theta * 180.0f / 3.14159265358979323846264f, axis);
    xform = rotation * xform;

    shader_program->Bind();  
    shader_program->SetUniform("xform", xform);
    shader_program->Unbind();  
  }
  
}

/* ------------------------------------------------------------------------- */

void FifthRenderer::OnDraw(float delta) {
  BaseRenderer::OnDraw(delta);

  shader_program->Bind();  
  plane->Bind();
  plane->DrawIndexed(GL_TRIANGLES);
  plane->Unbind();
  shader_program->Unbind();
}

/* ------------------------------------------------------------------------- */

void FifthRenderer::OnInitialize() {
  SetBackgroundColor(0.0f / 255, 0.0f / 255, 0.0f / 255, 1.0f);   

  glEnable(GL_DEPTH_TEST);

  int npositions;
  int nfaces;
  int *faces;
  float *positions;
  Vector4 *normals;
  read(MODELS_DIR "/suzanne.obj", npositions, positions, normals, nfaces, faces);

  // Ship positions off to GPU.
  plane_attributes = new VertexAttributes();
  plane_attributes->AddAttribute("position", npositions, 3, positions);
  plane_attributes->AddAttribute("normal", npositions, 4, &normals[0][0]);
  plane_attributes->AddIndices(nfaces * 3, faces);

  // CPU positions are free to go now.
  delete[] positions;
  delete[] faces;

  shader_program = ShaderProgram::FromFiles(SHADERS_DIR "/v.glsl", SHADERS_DIR "/f.glsl");

  xform = Matrix4::GetIdentity();
  shader_program->Bind();  
  shader_program->SetUniform("xform", xform);
  shader_program->SetUniform("projection", Matrix4::GetOrtho(-2, 2, -2, 2, -10, 10));
  shader_program->Unbind();  

  plane = new VertexArray(*shader_program, *plane_attributes);
}

/* ------------------------------------------------------------------------- */

void FifthRenderer::OnKey(Keys::key_t key) {
  switch (key) {
    case 'w':
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      break;
    case 'W':
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    default:
      BaseRenderer::OnKey(key);
      break;  
  }
}

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  FifthRenderer *renderer = new FifthRenderer();
  glut_render(renderer); 
  return 0;
}

/* ------------------------------------------------------------------------- */

// seventh
