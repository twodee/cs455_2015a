#version 120

attribute vec3 position;
attribute vec2 texcoords;
attribute vec4 normal;

uniform mat4 projection;
uniform mat4 xform;

varying vec3 fnormal;
varying vec2 ftexcoords;

void main() {
  gl_Position = projection * xform * vec4(position, 1.0);
  fnormal = (xform * normal).xyz;
  
  /* float radius = length(position.xz); */
  float theta = atan(position.z, position.x) + 3.1459;
  ftexcoords = vec2(1.0 - theta / (2.0 * 3.14159), (position.y + 0.5 * 2.631) / 2.631);
}

// ninth
