#include <cmath>
#include <iostream>
#include <fstream>

#include "BaseRenderer.h"
#include "glut_utilities.h"
#include "Image.h"
#include "Matrix4.h"
#include "ObjUtilities.h"
#include "ObjUtilities.h"
#include "VertexArray.h"

using namespace td;

/* ------------------------------------------------------------------------- */

class NinthRenderer : public BaseRenderer {
  public:
    void OnDraw(float delta);
    void OnInitialize();
    void OnKey(Keys::key_t key);
    void OnSizeChanged(int width, int height);
    void OnLeftMouseDown(int x, int y);
    void OnLeftMouseDragged(int x, int y);
    void OnLeftMouseUp(int x, int y);

  private:
    Vector4 ProjectOntoTrackball(int x_pixel, int y_pixel) const;

    VertexAttributes *plane_attributes;
    ShaderProgram *shader_program;
    VertexArray *plane;
    Texture *texture;

    Vector4 fore;
    Matrix4 xform;
};

/* ------------------------------------------------------------------------- */

Vector4 NinthRenderer::ProjectOntoTrackball(int x_pixel, int y_pixel) const {
  Vector4 v_pixel = Vector4(x_pixel, y_pixel, 0.0f, 1.0f); 
  Vector4 v_ball =
    Matrix4::GetTranslate(Vector4(-1.0f, -1.0f, 0.0f)) *
    Matrix4::GetScale(Vector4(2.0f / GetWidth(), 2.0f / GetHeight(), 1.0f)) *
    v_pixel;

  float z_squared = 1.0f - v_ball[0] * v_ball[0] - v_ball[1] * v_ball[1];
  if (z_squared < 0.0f) {
    z_squared = 0.0f;
  }

  v_ball[2] = sqrtf(z_squared);
  v_ball.Normalize();

  return v_ball;
}

/* ------------------------------------------------------------------------- */

void NinthRenderer::OnLeftMouseDown(int x, int y) {
  fore = ProjectOntoTrackball(x, y);
}

/* ------------------------------------------------------------------------- */

void NinthRenderer::OnLeftMouseDragged(int x, int y) {
  Vector4 aft = ProjectOntoTrackball(x, y);

  float theta = acos(fore.Dot(aft));
  if (fabs(theta) > 1.0e-6f) {
    Vector4 axis = fore.Cross(aft);
    axis.Normalize();

    Matrix4 rotation = Matrix4::GetRotate(theta * 180.0f / 3.14159265358979323846264f, axis);

    shader_program->Bind();  
    /* shader_program->SetUniform("xform", rotation * xform); */
    shader_program->SetUniform("xform", Matrix4::GetTranslate(Vector4(0, 0, -2, 0)) * rotation * xform);
    shader_program->Unbind();  
  }
}

/* ------------------------------------------------------------------------- */

void NinthRenderer::OnLeftMouseUp(int x, int y) {
  Vector4 aft = ProjectOntoTrackball(x, y);

  float theta = acos(fore.Dot(aft));
  if (fabs(theta) > 1.0e-6f) {
    Vector4 axis = fore.Cross(aft);
    axis.Normalize();

    Matrix4 rotation = Matrix4::GetRotate(theta * 180.0f / 3.14159265358979323846264f, axis);
    xform = rotation * xform;

    shader_program->Bind();  
    shader_program->SetUniform("xform", xform);
    shader_program->Unbind();  
  }
}

/* ------------------------------------------------------------------------- */

void NinthRenderer::OnDraw(float delta) {
  BaseRenderer::OnDraw(delta);

  shader_program->Bind();  
  shader_program->SetUniform("texture", *texture);
  plane->Bind();
  plane->DrawIndexed(GL_TRIANGLES);
  plane->Unbind();
  shader_program->Unbind();
}

/* ------------------------------------------------------------------------- */

void NinthRenderer::OnInitialize() {
  SetBackgroundColor(0.0f / 255, 0.0f / 255, 0.0f / 255, 1.0f);   

  glEnable(GL_DEPTH_TEST);

  int npositions;
  int nfaces;
  int *faces;
  float *positions;
  Vector4 *normals;
  float *texcoords;
  ObjUtilities::Read(MODELS_DIR "/can.obj", npositions, positions, texcoords, normals, nfaces, faces);

  Image image(MODELS_DIR "/label.ppm");

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  texture = new Texture(0);
  texture->Wrap(Texture::CLAMP_TO_EDGE);
  texture->Upload(image.GetWidth(), image.GetHeight(), image.GetPixels());
  OpenGL::CheckError("uploaded texture");

  // Ship positions off to GPU.
  plane_attributes = new VertexAttributes();
  plane_attributes->AddAttribute("position", npositions, 3, positions);
  plane_attributes->AddAttribute("normal", npositions, 4, &normals[0][0]);
  plane_attributes->AddIndices(nfaces * 3, faces);

  delete[] positions;
  delete[] faces;
  delete[] normals;

  shader_program = ShaderProgram::FromFiles(SHADERS_DIR "/v.glsl", SHADERS_DIR "/f.glsl");

  xform = Matrix4::GetIdentity();
  shader_program->Bind();
  shader_program->SetUniform("xform", Matrix4::GetTranslate(Vector4(0, 0, -2, 0)) * xform);
  shader_program->Unbind();  

  plane = new VertexArray(*shader_program, *plane_attributes);
}

/* ------------------------------------------------------------------------- */

void NinthRenderer::OnSizeChanged(int width, int height) {
  BaseRenderer::OnSizeChanged(width, height);

  Matrix4 projection;
  if (GetAspectRatio() > 1.0f) {
    projection = Matrix4::GetPerspective(GetAspectRatio() * -2.0f, GetAspectRatio() * 2.0f, -2.0f, 2.0f, 0.1f, 10.0f);
  } else {
    projection = Matrix4::GetPerspective(-2.0f, 2.0f, -2.0f / GetAspectRatio(), 2.0f / GetAspectRatio(), 0.1f, 10.0f);
  }

  shader_program->Bind();
  shader_program->SetUniform("projection", projection);
  shader_program->Unbind();  
}

/* ------------------------------------------------------------------------- */

void NinthRenderer::OnKey(Keys::key_t key) {
  switch (key) {
    case 'w':
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      break;
    case 'W':
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    default:
      BaseRenderer::OnKey(key);
      break;  
  }
}

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  NinthRenderer *renderer = new NinthRenderer();
  glut_render(renderer); 
  return 0;
}

/* ------------------------------------------------------------------------- */

// ninth
