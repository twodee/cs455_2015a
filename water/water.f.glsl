#version 120

uniform vec3 albedo = vec3(0.5, 0.5, 1.0);

void main() {
  gl_FragColor = vec4(albedo, 1.0);
}

// water
