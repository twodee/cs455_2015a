#version 120

uniform mat4 projection;
uniform mat4 modelview;
uniform mat4 model_to_world;

attribute vec3 position;
attribute vec2 texcoord;

void main() {
  vec4 position_eye = modelview * vec4(position, 1.0);
  gl_Position = projection * position_eye;
}

// water
