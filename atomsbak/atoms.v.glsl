#version 120

uniform mat4 projection;
uniform mat4 modelview;

attribute vec3 position;

void main() {
  /* gl_Position = projection * modelview * vec4(position, 1.0); */
  gl_Position = vec4(position, 1.0);
}

// atoms
