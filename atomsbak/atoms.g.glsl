#version 120
#extension GL_EXT_geometry_shader4 : enable

uniform mat4 projection;
uniform mat4 modelview;

varying vec2 ftexcoord;

void main() {
  float size = 0.05;
  vec4 right_eye = vec4(size, 0.0, 0.0, 0.0);
  vec4 up_eye = vec4(0.0, size, 0.0, 0.0);

  vec4 position_eye = modelview * gl_PositionIn[0];

  gl_Position = projection * (position_eye - right_eye - up_eye);
  ftexcoord = vec2(0.0, 0.0);
  EmitVertex();

  gl_Position = projection * (position_eye + right_eye - up_eye);
  ftexcoord = vec2(1.0, 0.0);
  EmitVertex();

  gl_Position = projection * (position_eye - right_eye + up_eye);
  ftexcoord = vec2(0.0, 1.0);
  EmitVertex();

  gl_Position = projection * (position_eye + right_eye + up_eye);
  ftexcoord = vec2(1.0, 1.0);
  EmitVertex();

  EndPrimitive();
}

// atoms
