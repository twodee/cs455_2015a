#version 120

uniform sampler2D tex;
uniform vec3 albedo = vec3(0.5, 0.9, 0.2);

const float diffuse_weight = 0.7;
const vec3 light_color = vec3(1.0);
const vec3 light_direction = normalize(vec3(0.0, 0.0, 1.0));

varying vec3 fnormal;
varying vec3 fposition_eye;

void main() {
  vec3 normal = normalize(fnormal);
  float litness = max(dot(normal, light_direction), 0.0);
  vec3 diffuse_color = litness * light_color * albedo;
  vec3 ambient_color = light_color * albedo;
  vec3 halfway = normalize(light_direction - normalize(fposition_eye)); 
  float n_dot_h = max(dot(normal, halfway), 0.0) * step(0.0, dot(normal, light_direction));
  vec3 specular_color = pow(n_dot_h, 50.0) * vec3(1.0);
  vec3 color = specular_color + diffuse_weight * diffuse_color + (1.0 - diffuse_weight) * ambient_color;
  gl_FragColor = vec4(color, 1.0);
}

// atoms
