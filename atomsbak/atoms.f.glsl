#version 120

uniform sampler2D atom_texture;

varying vec2 ftexcoord;

void main() {
  gl_FragColor = texture2D(atom_texture, ftexcoord);
  if (gl_FragColor.a < 0.5) discard;
}

// atoms
