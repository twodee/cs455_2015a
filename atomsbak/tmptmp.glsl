#version 120

uniform mat4 projection;
uniform mat4 modelview;

attribute vec3 position;

void main() {
  /* gl_Position = projection * modelview * vec4(position, 1.0); */
  gl_Position = vec4(position, 1.0);
}

// atoms
#version 120
#extension GL_EXT_geometry_shader4 : enable

uniform mat4 projection;
uniform mat4 modelview;

varying vec2 ftexcoord;

void main() {
  float size = 0.05;
  vec4 right_eye = vec4(size, 0.0, 0.0, 0.0);
  vec4 up_eye = vec4(0.0, size, 0.0, 0.0);

  vec4 position_eye = modelview * gl_PositionIn[0];

  gl_Position = projection * (position_eye - right_eye - up_eye);
  ftexcoord = vec2(0.0, 0.0);
  EmitVertex();

  gl_Position = projection * (position_eye + right_eye - up_eye);
  ftexcoord = vec2(1.0, 0.0);
  EmitVertex();

  gl_Position = projection * (position_eye - right_eye + up_eye);
  ftexcoord = vec2(0.0, 1.0);
  EmitVertex();

  gl_Position = projection * (position_eye + right_eye + up_eye);
  ftexcoord = vec2(1.0, 1.0);
  EmitVertex();

  EndPrimitive();
}

// atoms
#version 120

uniform sampler2D atom_texture;

varying vec2 ftexcoord;

void main() {
  gl_FragColor = texture2D(atom_texture, ftexcoord);
  if (gl_FragColor.a < 0.5) discard;
}

// atoms
