#include <cmath>
#include <iostream>
#include <fstream>

#include "BaseRenderer.h"
#include "glut_utilities.h"
#include "Image.h"
#include "Matrix4.h"
#include "ObjUtilities.h"
#include "ObjUtilities.h"
#include "VertexArray.h"

using namespace td;

/* ------------------------------------------------------------------------- */

class TenthRenderer : public BaseRenderer {
  public:
    void OnDraw(float delta);
    void OnInitialize();
    void OnKey(Keys::key_t key);
    void OnSizeChanged(int width, int height);
    void OnLeftMouseDown(int x, int y);
    void OnLeftMouseDragged(int x, int y);
    void OnLeftMouseUp(int x, int y);

  private:
    Vector4 ProjectOntoTrackball(int x_pixel, int y_pixel) const;

    VertexAttributes *plane_attributes;
    ShaderProgram *shader_program;
    VertexArray *plane;
    Texture *texture;

    Vector4 fore;
    Matrix4 xform;
};

/* ------------------------------------------------------------------------- */

Vector4 TenthRenderer::ProjectOntoTrackball(int x_pixel, int y_pixel) const {
  Vector4 v_pixel = Vector4(x_pixel, y_pixel, 0.0f, 1.0f); 
  Vector4 v_ball =
    Matrix4::GetTranslate(Vector4(-1.0f, -1.0f, 0.0f)) *
    Matrix4::GetScale(Vector4(2.0f / GetWidth(), 2.0f / GetHeight(), 1.0f)) *
    v_pixel;

  float z_squared = 1.0f - v_ball[0] * v_ball[0] - v_ball[1] * v_ball[1];
  if (z_squared < 0.0f) {
    z_squared = 0.0f;
  }

  v_ball[2] = sqrtf(z_squared);
  v_ball.Normalize();

  return v_ball;
}

/* ------------------------------------------------------------------------- */

void TenthRenderer::OnLeftMouseDown(int x, int y) {
  fore = ProjectOntoTrackball(x, y);
}

/* ------------------------------------------------------------------------- */

void TenthRenderer::OnLeftMouseDragged(int x, int y) {
  Vector4 aft = ProjectOntoTrackball(x, y);

  float theta = acos(fore.Dot(aft));
  if (fabs(theta) > 1.0e-6f) {
    Vector4 axis = fore.Cross(aft);
    axis.Normalize();

    Matrix4 rotation = Matrix4::GetRotate(theta * 180.0f / 3.14159265358979323846264f, axis);

    shader_program->Bind();  
    shader_program->SetUniform("xform", rotation * xform);
    shader_program->Unbind();  
  }
}

/* ------------------------------------------------------------------------- */

void TenthRenderer::OnLeftMouseUp(int x, int y) {
  Vector4 aft = ProjectOntoTrackball(x, y);

  float theta = acos(fore.Dot(aft));
  if (fabs(theta) > 1.0e-6f) {
    Vector4 axis = fore.Cross(aft);
    axis.Normalize();

    Matrix4 rotation = Matrix4::GetRotate(theta * 180.0f / 3.14159265358979323846264f, axis);
    xform = rotation * xform;

    shader_program->Bind();  
    shader_program->SetUniform("xform", xform);
    shader_program->Unbind();  
  }
}

/* ------------------------------------------------------------------------- */

void TenthRenderer::OnDraw(float delta) {
  BaseRenderer::OnDraw(delta);

  shader_program->Bind();  
  shader_program->SetUniform("texture", *texture);
  plane->Bind();
  plane->DrawIndexed(GL_TRIANGLES);
  plane->Unbind();
  shader_program->Unbind();
}

/* ------------------------------------------------------------------------- */

void TenthRenderer::OnInitialize() {
  SetBackgroundColor(0.0f / 255, 0.0f / 255, 0.0f / 255, 1.0f);   

  glEnable(GL_DEPTH_TEST);

  int nvertices;
  int nfaces;
  int *faces;
  float *positions;
  float *texcoords;
  Vector4 *normals;
  ObjUtilities::Read(MODELS_DIR "/quad.obj", nvertices, positions, texcoords, normals, nfaces, faces);
  std::cout << "positions[#](# in 0,9): " << positions[0] << ", " << positions[1] << ", " << positions[2] << ", " << positions[3] << ", " << positions[4] << ", " << positions[5] << ", " << positions[6] << ", " << positions[7] << ", " << positions[8] << std::endl;
  std::cout << "normals[#](# in 0,3): " << normals[0] << ", " << normals[1] << ", " << normals[2] << std::endl;
  std::cout << "faces[#](# in 0,3): " << faces[0] << ", " << faces[1] << ", " << faces[2] << std::endl;
  std::cout << "texcoords[#](# in 0,6): " << texcoords[0] << ", " << texcoords[1] << ", " << texcoords[2] << ", " << texcoords[3] << ", " << texcoords[4] << ", " << texcoords[5] << std::endl;

  Image image(MODELS_DIR "/label.ppm");

  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  texture = new Texture(0);
  texture->Wrap(Texture::CLAMP_TO_EDGE);
  texture->Upload(image.GetWidth(), image.GetHeight(), image.GetPixels());
  OpenGL::CheckError("uploaded texture");

  // Ship positions off to GPU.
  plane_attributes = new VertexAttributes();
  plane_attributes->AddAttribute("position", nvertices, 3, positions);
  plane_attributes->AddAttribute("normal", nvertices, 4, &normals[0][0]);
  plane_attributes->AddAttribute("texcoords", nvertices, 2, texcoords);
  plane_attributes->AddIndices(nfaces * 3, faces);

  delete[] positions;
  delete[] faces;
  delete[] normals;

  shader_program = ShaderProgram::FromFiles(SHADERS_DIR "/v.glsl", SHADERS_DIR "/f.glsl");

  xform = Matrix4::GetIdentity();
  shader_program->Bind();
  shader_program->SetUniform("xform", xform);
  shader_program->Unbind();  

  plane = new VertexArray(*shader_program, *plane_attributes);
}

/* ------------------------------------------------------------------------- */

void TenthRenderer::OnSizeChanged(int width, int height) {
  BaseRenderer::OnSizeChanged(width, height);

  Matrix4 projection;
  if (GetAspectRatio() > 1.0f) {
    projection = Matrix4::GetOrtho(GetAspectRatio() * -2.0f, GetAspectRatio() * 2.0f, -2.0f, 2.0f, -10.0f, 10.0f);
  } else {
    projection = Matrix4::GetOrtho(-2.0f, 2.0f, -2.0f / GetAspectRatio(), 2.0f / GetAspectRatio(), -10.0f, 10.0f);
  }

  shader_program->Bind();
  shader_program->SetUniform("projection", projection);
  shader_program->Unbind();  
}

/* ------------------------------------------------------------------------- */

void TenthRenderer::OnKey(Keys::key_t key) {
  switch (key) {
    case 'w':
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      break;
    case 'W':
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    default:
      BaseRenderer::OnKey(key);
      break;  
  }
}

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  TenthRenderer *renderer = new TenthRenderer();
  glut_render(renderer); 
  return 0;
}

/* ------------------------------------------------------------------------- */

// tenth
