#version 120

attribute vec3 position;
attribute vec4 normal;

uniform mat4 projection;
uniform mat4 modelview;

varying vec3 fnormal;
varying vec3 fposition_eye;

void main() {
  gl_Position = projection * modelview * vec4(position, 1.0);
  fposition_eye = (modelview * vec4(position, 1.0)).xyz;
  fnormal = (modelview * normal).xyz;
}

// atoms
