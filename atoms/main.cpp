#include <cmath>
#include <iostream>
#include <fstream>

#include "BaseRenderer.h"
#include "Camera.h"
#include "glut_utilities.h"
#include "FramebufferObject.h"
#include "Matrix4.h"
#include "ObjUtilities.h"
#include "VertexArray.h"

using namespace td;

/* ------------------------------------------------------------------------- */

class AtomsRenderer : public BaseRenderer {
  public:
    void OnDraw(float delta);
    void OnInitialize();
    void OnKey(Keys::key_t key);
    void OnSizeChanged(int width, int height);
    void OnMouseHover(int x, int y);
    void OnMousePass(bool is_entering);

  private:
    VertexAttributes *attributes[2];
    ShaderProgram *shader_programs[2];
    VertexArray *atoms;
    VertexArray *atom;
    Texture *atom_texture;
    Camera camera;
    int mouse_at[2];
};

/* ------------------------------------------------------------------------- */

void AtomsRenderer::OnDraw(float delta) {
  BaseRenderer::OnDraw(delta);

  // TODO draw atom points
  shader_programs[0]->Bind();
  shader_programs[0]->SetUniform("modelview", camera.GetViewMatrix());
  shader_programs[0]->SetUniform("atom_texture", *atom_texture);

  atoms->Bind();
  atoms->DrawSequence(GL_POINTS);
  atoms->Unbind();

  shader_programs[0]->Unbind();

#if 0
  // Test
  shader_programs[1]->Bind();
  shader_programs[1]->SetUniform("projection", Matrix4::GetOrtho(-1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 20.0f));
  shader_programs[1]->SetUniform("modelview", Matrix4::GetTranslate(Vector4(0.0f, 0.0f, -10.0f)));

  atom->Bind();
  atom->DrawIndexed(GL_TRIANGLES);
  atom->Unbind();

  shader_programs[1]->Unbind();
#endif
}

/* ------------------------------------------------------------------------- */

void AtomsRenderer::OnInitialize() {
  SetBackgroundColor(0.2f, 0.0f, 0.4f, 1.0f);
  /* SetBackgroundColor(0.0f, 0.0f, 0.0f, 1.0f); */
  glLineWidth(5.0f);
  glPointSize(5.0f);

  glDepthFunc(GL_LEQUAL);
  glEnable(GL_DEPTH_TEST);

  // TODO create atom points
#define NATOMS 3000
  float positions[NATOMS * 3];
  float *position = positions;
  float size = 2.0f;
  for (int i = 0; i < NATOMS; ++i, position += 3) {
    position[0] = size * rand() / (float) RAND_MAX - size * 0.5f;
    position[1] = size * rand() / (float) RAND_MAX - size * 0.5f;
    position[2] = size * rand() / (float) RAND_MAX - size * 0.5f;
  }

  attributes[0] = new VertexAttributes();
  attributes[0]->AddAttribute("position", NATOMS, 3, positions);
  
  shader_programs[0] = ShaderProgram::FromFiles(SHADERS_DIR "/atoms.v.glsl",
                                                SHADERS_DIR "/atoms.f.glsl",
                                                SHADERS_DIR "/atoms.g.glsl");

  atoms = new VertexArray(*shader_programs[0], *attributes[0]);

  // TODO create highly-faceted atom geometry
  attributes[1] = ObjUtilities::Read(MODELS_DIR "/unit_sphere.obj");
  shader_programs[1] = ShaderProgram::FromFiles(SHADERS_DIR "/v.glsl",
                                                SHADERS_DIR "/f.glsl");
  atom = new VertexArray(*shader_programs[1], *attributes[1]);

  // TODO generate texture
  atom_texture = new Texture(); 
  atom_texture->Channels(Texture::RGBA);
  atom_texture->Allocate(512, 512);

  // TODO generate framebuffer object
  FramebufferObject *fbo = new FramebufferObject(atom_texture);

  // TODO render to texture
  fbo->Bind();
  glViewport(0, 0, fbo->GetWidth(), fbo->GetHeight());
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  shader_programs[1]->Bind();
  shader_programs[1]->SetUniform("projection", Matrix4::GetOrtho(-1.0f, 1.0f, -1.0f, 1.0f, 0.0f, 20.0f));
  shader_programs[1]->SetUniform("modelview", Matrix4::GetTranslate(Vector4(0.0f, 0.0f, -10.0f, 0.0f)));

  atom->Bind();
  atom->DrawIndexed(GL_TRIANGLES);
  atom->Unbind();

  shader_programs[1]->Unbind();

  fbo->Unbind();
 
  camera.LookAt(Vector4(0.0f), 
                Vector4(0.0f, 0.0f, -10.0f),
                Vector4(0.0f, 1.0f, 0.0f, 0.0f));

  OpenGL::CheckError("finished oninitialize");
}

/* ------------------------------------------------------------------------- */

void AtomsRenderer::OnSizeChanged(int width, int height) {
  BaseRenderer::OnSizeChanged(width, height);

  Matrix4 projection = Matrix4::GetPerspective(45.0f, GetAspectRatio(), 0.1f, 400.0f);

  for (int i = 0; i < 1; ++i) {
    shader_programs[i]->Bind();
    shader_programs[i]->SetUniform("projection", projection);
    shader_programs[i]->Unbind();  
  }

  OpenGL::CheckError("on size changed");
}

/* ------------------------------------------------------------------------- */

void AtomsRenderer::OnKey(Keys::key_t key) {
  switch (key) {
    case '|':
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      break;
    case '.':
      glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
      break;
    case '3':
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    case 'w':
      camera.Advance(0.1f);
      break;
    case 's':
      camera.Advance(-0.1f);
      break;
    case 'd':
      camera.Strafe(1.0f);
      break;
    case 'a':
      camera.Strafe(-1.0f);
      break;
    case 'q':
      camera.Yaw(3.0f);
      break;
    case 'e':
      camera.Yaw(-3.0f);
      break;
    case '[':
      camera.Roll(-3.0f);
      break;
    case ']':
      camera.Roll(3.0f);
      break;
    case '{':
      camera.Pitch(-3.0f);
      break;
    case '}':
      camera.Pitch(3.0f);
      break;
    default:
      BaseRenderer::OnKey(key);
      break;  
  }
}

/* ------------------------------------------------------------------------- */

void AtomsRenderer::OnMouseHover(int x, int y) {
  if (mouse_at[0] != -1) {
    int diff_x = x - mouse_at[0];
    camera.Yaw(diff_x / -2.0f);
  }
  
  if (mouse_at[1] != -1) {
    int diff_y = y - mouse_at[1];
    camera.Pitch(diff_y / 10.0f);
  }

  mouse_at[0] = x;
  mouse_at[1] = y;
}

/* ------------------------------------------------------------------------- */

void AtomsRenderer::OnMousePass(bool is_entering) {
  if (is_entering) {
    mouse_at[0] = mouse_at[1] = -1;
  }
}

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  AtomsRenderer *renderer = new AtomsRenderer();
  glut_render(renderer); 
  return 0;
}

/* ------------------------------------------------------------------------- */

// atoms
