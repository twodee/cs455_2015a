#version 120

attribute vec3 position;

uniform float theta;

void main() {
  gl_Position = vec4(position.x * cos(theta) - position.z * sin(theta),
                     position.y,
                     position.x * sin(theta) + position.z * cos(theta), 1.0);
}

// third
