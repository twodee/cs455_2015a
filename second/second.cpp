#include <iostream>

#include "BaseRenderer.h"
#include "glut_utilities.h"
#include "VertexArray.h"

using namespace td;

/* ------------------------------------------------------------------------- */

class SecondRenderer : public BaseRenderer {
  public:
    void OnDraw(float delta);
    void OnInitialize();
    void OnKey(Keys::key_t key);
    void OnLeftMouseDown(int x, int y);
    void OnLeftMouseDragged(int x, int y);

  private:
    VertexAttributes *plane_attributes;
    ShaderProgram *shader_program;
    VertexArray *plane;
    float point_size;
    int mouse_x;
    int mouse_y;
    float shift_x;
    float shift_y;
};

/* ------------------------------------------------------------------------- */

void SecondRenderer::OnLeftMouseDown(int x, int y) {
  mouse_x = x;
  mouse_y = y; 
}

/* ------------------------------------------------------------------------- */

void SecondRenderer::OnLeftMouseDragged(int x, int y) {
  shift_x += 2.0f * (x - mouse_x) / GetWidth(); 
  shift_y += 2.0f * (y - mouse_y) / GetHeight(); 
  mouse_x = x;
  mouse_y = y;
}

/* ------------------------------------------------------------------------- */

void SecondRenderer::OnDraw(float delta) {
  /* glClear(GL_COLOR_BUFFER_BIT); */
  BaseRenderer::OnDraw(delta);

  shader_program->Bind();  
  // TODO use shift_x and shift_y
  shader_program->SetUniform("shift", shift_x, shift_y);
  plane->Bind();
  plane->DrawSequence(GL_POINTS);
  plane->Unbind();
  shader_program->Unbind();
}

/* ------------------------------------------------------------------------- */

void SecondRenderer::OnInitialize() {
  SetBackgroundColor(175.0f / 255, 238.0f / 255, 238.0f / 255, 1.0f);   

  point_size = 1.0f;
  shift_x = shift_y = 1.0f;

  int width = 32;
  int height = 22;

  float *positions = new float[width * height * 2];
  float *position = positions;
  for (int r = 0; r < height; ++r) {
    for (int c = 0; c < width; ++c) {
      position[0] = -1 + c * 0.1;
      position[1] = -1 + r * 0.1;
      position += 2;
    }
  }

  // Ship positions off to GPU.
  plane_attributes = new VertexAttributes();
  plane_attributes->AddAttribute("position", width * height, 2, positions);

  // CPU positions are free to go now.
  delete[] positions;

  shader_program = ShaderProgram::FromFiles(SHADERS_DIR "/passthru.v.glsl", SHADERS_DIR "/white.f.glsl");

  plane = new VertexArray(*shader_program, *plane_attributes);
}

/* ------------------------------------------------------------------------- */

void SecondRenderer::OnKey(Keys::key_t key) {
  switch (key) {
    case '+':
      point_size += 1.0f;
      glPointSize(point_size);
      break;
    case '-':
      point_size -= 1.0f;
      glPointSize(point_size);
      break;
    default:
      BaseRenderer::OnKey(key);
      break;  
  }
}

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  SecondRenderer *renderer = new SecondRenderer();
  glut_render(renderer); 
  return 0;
}

/* ------------------------------------------------------------------------- */


// second
