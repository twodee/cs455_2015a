add_definitions(-DSHADERS_DIR="${CMAKE_CURRENT_SOURCE_DIR}")
add_executable(second second.cpp ${COMMON})
target_link_libraries(second ${GLUT_LIBRARIES} ${GLEW_LIBRARY} ${OPENGL_LIBRARIES})

if(WIN32)
  add_custom_command(
      TARGET second POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy_if_different
      "${GLUT_DLL}"
      $<TARGET_FILE_DIR:second>)
    
  add_custom_command(
      TARGET second POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy_if_different
      "${GLEW_DLL}"
      $<TARGET_FILE_DIR:second>)
endif(WIN32)
