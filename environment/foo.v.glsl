#version 120

uniform mat4 projection;
uniform mat4 modelview;

uniform mat4 model_to_world;
uniform vec3 camera_world;

attribute vec3 position;
attribute vec3 normal;

varying vec3 ftexcoord;

void main() {
  vec4 position_eye = modelview * vec4(position, 1.0);
  gl_Position = projection * position_eye;

  vec3 normal_world = (model_to_world * vec4(normal, 0.0)).xyz;
  vec3 position_world = (model_to_world * vec4(position, 1.0)).xyz;
  vec3 incident = normalize(position_world - camera_world);

  ftexcoord = reflect(incident, normal_world);
}

// environment
