#version 120

uniform mat4 projection;
uniform mat4 modelview;

attribute vec3 position;

varying vec3 ftexcoord;

void main() {
  vec4 position_eye = modelview * vec4(position, 1.0);
  gl_Position = projection * position_eye;

  gl_Position.z = gl_Position.w;

  ftexcoord = position;
}

// environment
