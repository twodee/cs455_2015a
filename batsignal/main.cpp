#include <cmath>
#include <iostream>
#include <fstream>

#include "BaseRenderer.h"
#include "Camera.h"
#include "glut_utilities.h"
#include "Image.h"
#include "Matrix4.h"
#include "ObjUtilities.h"
#include "VertexArray.h"

using namespace td;

/* ------------------------------------------------------------------------- */

class BatSignalRenderer : public BaseRenderer {
  public:
    void OnDraw(float delta);
    void OnInitialize();
    void OnKey(Keys::key_t key);
    void OnSizeChanged(int width, int height);
    void OnMouseHover(int x, int y);
    void OnMousePass(bool is_entering);

  private:
    VertexAttributes *attributes[2];
    ShaderProgram *shader_programs[2];
    VertexArray *box;
    VertexArray *cone;
    Texture *bat_signal_texture;
    Camera camera;
    Camera light_camera;
    int mouse_at[2];
};

/* ------------------------------------------------------------------------- */

void BatSignalRenderer::OnDraw(float delta) {
  BaseRenderer::OnDraw(delta);

  // --------------------------------------------------------------------------
  shader_programs[0]->Bind();
  shader_programs[0]->SetUniform("modelview", camera.GetViewMatrix());
  shader_programs[0]->SetUniform("bat_signal_texture", *bat_signal_texture);

  Matrix4 world_to_tex = Matrix4::GetTranslate(Vector4(0.5f, 0.5f, 0.5f)) *
                         Matrix4::GetScale(Vector4(0.5f, 0.5f, 0.5f)) *
                         Matrix4::GetPerspective(80.0f, 2.0f, 0.1f, 1000.0f) *
                         light_camera.GetViewMatrix();

  shader_programs[0]->SetUniform("object_to_tex", world_to_tex * Matrix4::GetIdentity());

  box->Bind();
  box->DrawIndexed(GL_TRIANGLES);
  box->Unbind();

  shader_programs[0]->Unbind();

  // --------------------------------------------------------------------------
  shader_programs[1]->Bind();
  shader_programs[1]->SetUniform("modelview", camera.GetViewMatrix() * light_camera.GetViewMatrix().GetTranspose());

  cone->Bind();
  cone->DrawIndexed(GL_TRIANGLES);
  cone->Unbind();

  shader_programs[1]->Unbind();
}

/* ------------------------------------------------------------------------- */

void BatSignalRenderer::OnInitialize() {
  SetBackgroundColor(0.2f, 0.0f, 0.4f, 1.0f);
  glLineWidth(5.0f);
  glPointSize(5.0f);

  glDepthFunc(GL_LEQUAL);
  glEnable(GL_DEPTH_TEST);

  attributes[0] = ObjUtilities::Read(MODELS_DIR "/inward_box.obj");
  shader_programs[0] = ShaderProgram::FromFiles(SHADERS_DIR "/v.glsl",
                                                SHADERS_DIR "/f.glsl");
  box = new VertexArray(*shader_programs[0], *attributes[0]);

  attributes[1] = ObjUtilities::Read(MODELS_DIR "/cone.obj");
  shader_programs[1] = ShaderProgram::FromFiles(SHADERS_DIR "/spot.v.glsl",
                                                SHADERS_DIR "/spot.f.glsl");
  cone = new VertexArray(*shader_programs[1], *attributes[1]);

  // --------------------------------------------------------------------------
  // TODO generate texture
  Image image(MODELS_DIR "/batsignal.ppm");
  bat_signal_texture = new Texture(); 
  bat_signal_texture->Channels(Texture::RGB);
  bat_signal_texture->Wrap(Texture::CLAMP_TO_BORDER);
  bat_signal_texture->Upload(image.GetWidth(), image.GetHeight(), image.GetPixels());
 
  camera.LookAt(Vector4(0.0f, 0.0f, -1.0f), 
                Vector4(0.0f, 0.0f, -10.0f),
                Vector4(0.0f, 1.0f, 0.0f, 0.0f));

  light_camera.LookAt(Vector4(0.0f, 0.0f, 0.0f),
                      Vector4(0.0f, 0.0f, -1.0f),
                      Vector4(0.0f, 1.0f, 0.0f, 0.0f));

  OpenGL::CheckError("finished oninitialize");
}

/* ------------------------------------------------------------------------- */

void BatSignalRenderer::OnSizeChanged(int width, int height) {
  BaseRenderer::OnSizeChanged(width, height);

  Matrix4 projection = Matrix4::GetPerspective(45.0f, GetAspectRatio(), 0.1f, 400.0f);

  for (int i = 0; i < 2; ++i) {
    shader_programs[i]->Bind();
    shader_programs[i]->SetUniform("projection", projection);
    shader_programs[i]->Unbind();  
  }

  OpenGL::CheckError("on size changed");
}

/* ------------------------------------------------------------------------- */

void BatSignalRenderer::OnKey(Keys::key_t key) {
  switch (key) {
    case Keys::LEFT:
      light_camera.Yaw(3.0f);
      break;
    case Keys::RIGHT:
      light_camera.Yaw(-3.0f);
      break;
    case Keys::UP:
      light_camera.Pitch(3.0f);
      break;
    case Keys::DOWN:
      light_camera.Pitch(-3.0f);
      break;
    case '|':
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      break;
    case '.':
      glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
      break;
    case '3':
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    case 'w':
      camera.Advance(0.1f);
      break;
    case 's':
      camera.Advance(-0.1f);
      break;
    case 'd':
      camera.Strafe(0.1f);
      break;
    case 'a':
      camera.Strafe(-0.1f);
      break;
    case 'q':
      camera.Yaw(3.0f);
      break;
    case 'e':
      camera.Yaw(-3.0f);
      break;
    case '[':
      camera.Roll(-3.0f);
      break;
    case ']':
      camera.Roll(3.0f);
      break;
    case '{':
      camera.Pitch(-3.0f);
      break;
    case '}':
      camera.Pitch(3.0f);
      break;
    default:
      BaseRenderer::OnKey(key);
      break;  
  }
}

/* ------------------------------------------------------------------------- */

void BatSignalRenderer::OnMouseHover(int x, int y) {
  if (mouse_at[0] != -1) {
    int diff_x = x - mouse_at[0];
    camera.Yaw(diff_x / -2.0f);
  }
  
  if (mouse_at[1] != -1) {
    int diff_y = y - mouse_at[1];
    camera.Pitch(diff_y / 10.0f);
  }

  mouse_at[0] = x;
  mouse_at[1] = y;
}

/* ------------------------------------------------------------------------- */

void BatSignalRenderer::OnMousePass(bool is_entering) {
  if (is_entering) {
    mouse_at[0] = mouse_at[1] = -1;
  }
}

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  BatSignalRenderer *renderer = new BatSignalRenderer();
  glut_render(renderer); 
  return 0;
}

/* ------------------------------------------------------------------------- */

// batsignal
