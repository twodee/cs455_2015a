#include <cassert>
#include <fstream>
#include <iostream>

#include "Image.h"

namespace td {

/* ------------------------------------------------------------------------- */

Image::Image(const std::string& path) {
  std::ifstream in(path);

  if (in.fail()) {
    std::cerr << "bad path: " << path << std::endl;
    exit(1);
  }

  std::string magic_number;

  int max;
  in >> magic_number >> width >> height >> max;
  nchannels = magic_number[1] - '0' == 3 ? 3 : 1;
  std::cout << "nchannels: " << nchannels << std::endl;

  pixels = new unsigned char[width * height * nchannels];
  for (int r = 0; r < height; ++r) {
    for (int c = 0; c < width; ++c) {
      for (int i = 0; i < nchannels; ++i) {
        int value;
        in >> value;
        pixels[(c + (height - 1 - r) * width) * nchannels + i] = value;
      }
    }
  }
  in.close();
}

/* ------------------------------------------------------------------------- */

void Image::WriteToFile(const std::string& path) {
  std::ofstream out(path);
  out << (GetChannelCount() == 1 ? "P2" : "P3") << std::endl;
  out << GetWidth() << " " << GetHeight() << std::endl << 255 << std::endl;
  for (int r = 0; r < GetHeight(); ++r) {
    for (int c = 0; c < GetWidth(); ++c) {
      for (int i = 0; i < GetChannelCount(); ++i) {
        out << (int) (*this)(c, r, i) << std::endl;
      }
    }
  }
  out.close();
}

/* ------------------------------------------------------------------------- */

int Image::GetWidth() const {
  return width;
}

/* ------------------------------------------------------------------------- */

int Image::GetHeight() const {
  return height;
}

/* ------------------------------------------------------------------------- */

const unsigned char *Image::GetPixels() const {
  return pixels;
}

/* ------------------------------------------------------------------------- */

int Image::GetChannelCount() const {
  return nchannels;
}

/* ------------------------------------------------------------------------- */

unsigned char *Image::operator()(int c, int r) {
  assert(c >= 0 && c < GetWidth());
  assert(r >= 0 && r < GetHeight());
  return &pixels[(c + r * GetWidth()) * GetChannelCount()];
}

/* ------------------------------------------------------------------------- */

unsigned char& Image::operator()(int c, int r, int channel) {
  if (c >= GetWidth()) c = GetWidth() - 1;
  if (r >= GetHeight()) r = GetHeight() - 1;
  return pixels[(c + r * GetWidth()) * GetChannelCount() + channel];
}

/* ------------------------------------------------------------------------- */

const unsigned char& Image::operator()(int c, int r, int channel) const {
  if (c >= GetWidth()) c = GetWidth() - 1;
  if (r >= GetHeight()) r = GetHeight() - 1;
  return pixels[(c + r * GetWidth()) * GetChannelCount() + channel];
}

/* ------------------------------------------------------------------------- */

Image::Image(int width, int height, int nchannels) {
  this->width = width;
  this->height = height;
  this->nchannels = nchannels;
  pixels = new unsigned char[width * height * nchannels];
}

/* ------------------------------------------------------------------------- */

}
