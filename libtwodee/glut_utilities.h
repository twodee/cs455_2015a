#ifndef GLUT_WINDOW_H
#define GLUT_WINDOW_H

#include "BaseRenderer.h"

void glut_render(td::BaseRenderer *given_renderer);
void glut_cursor_off();
void glut_cursor_on();
void glut_cursor_warp(int x, int y);

#endif
