#ifndef CAMERA_H
#define CAMERA_H

#include "Vector4.h"
#include "Matrix4.h"

namespace td {

/* ------------------------------------------------------------------------- */

class Camera {
  public:
    void LookAt(const Vector4& from,
                const Vector4& to,
                const Vector4& up);
    const Matrix4& GetViewMatrix() const;
    void Advance(float offset);
    void Strafe(float offset);
    void Roll(float degrees);
    void Yaw(float degrees);
    void Pitch(float degrees);
    const Vector4& GetFrom() const;
    const Vector4& GetTo() const;
    void SetHeight(float height);

  private:
    Vector4 from;
    Vector4 to;
    Vector4 up;
    Vector4 forward;
    Vector4 right;

    Matrix4 view_matrix;
};

/* ------------------------------------------------------------------------- */

}
#endif
