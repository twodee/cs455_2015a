#include <cassert>
#include <fstream>
#include <iostream>

#include "Field2.h"

/* ------------------------------------------------------------------------- */

Field2::Field2() :
  width(0),
  height(0),
  intensities(NULL) {
}

/* ------------------------------------------------------------------------- */

Field2::Field2(int width, int height) :
  width(width),
  height(height),
  intensities(new float[width * height]) {
  for (int r = 0; r < height; ++r) {
    for (int c = 0; c < width; ++c) {
      (*this)(c, r) = 0.0f;
    }
  }
}

/* ------------------------------------------------------------------------- */

Field2::~Field2() {
  delete[] intensities;  
}

/* ------------------------------------------------------------------------- */

void Field2::WriteToPGM(const std::string& path) {
  // Write intensities out to file in PGM format. Assume intensites are in
  // [0, 1]. Scale up to [0, 255] to get grayscale value.

  std::ofstream out(path.c_str());

  out << "P2" << std::endl;
  out << width << " " << height << std::endl;
  out << 255 << std::endl;

  for (int r = 0; r < height; ++r) {
    for (int c = 0; c < width; ++c) {
      out << (int) ((*this)(c, r) * 255) << std::endl;
    }
  }

  out.close();
}

/* ------------------------------------------------------------------------- */

int Field2::GetWidth() const {
  return width;
}

/* ------------------------------------------------------------------------- */

int Field2::GetHeight() const {
  return height;
}

/* ------------------------------------------------------------------------- */

const float *Field2::GetIntensities() const {
  return intensities;
}

/* ------------------------------------------------------------------------- */

float &Field2::operator()(int c, int r) {
  // TODO: Get intensity at location (c, r). If either c or r is out of bounds,
  // clamp it.
  if (c > width - 1) {
    c = c - 1;
  } else if (c < 0) {
    c = 0;
  }

  if (r > height - 1) {
    r = r - 1;
  } else if (r < 0) {
    r = 0;
  }

  return intensities[c + r * width];
}

/* ------------------------------------------------------------------------- */

const float &Field2::operator()(int c, int r) const {
  // TODO: Get intensity at location (c, r). If either c or r is out of bounds,
  // clamp it.

  if (c > width - 1) {
    c = c - 1;
  } else if (c < 0) {
    c = 0;
  }

  if (r > height - 1) {
    r = r - 1;
  } else if (r < 0) {
    r = 0;
  }

  return intensities[c + r * width];
}

/* ------------------------------------------------------------------------- */

float Field2::Interpolate(float x, float y) const {
  // Find integral corner.
  int ix = (int) x;
  int iy = (int) y;

  // Find fraction away.
  float fx = x - ix;
  float fy = y - iy;

  float bl = (*this)(ix, iy);
  float br = (*this)(ix + 1, iy);
  float tl = (*this)(ix, iy + 1);
  float tr = (*this)(ix + 1, iy + 1);

  float b = fx * br + (1 - fx) * bl;
  float t = fx * tr + (1 - fx) * tl;
  float intensity = fy * t + (1 - fy) * b;

  return intensity;
}

/* ------------------------------------------------------------------------- */

Field2 *Field2::Scale(int new_width, int new_height) {
  // TODO: Create a new Field2 that's a scaled version of this one. You'll
  // need to assign each intensity in the new field. Consider each location's
  // as a proportion within the field's bounds. Apply that proportion to the
  // original field, and use Interpolate to get the intensity at that location.

  Field2 *scaled = new Field2;
  scaled->width = new_width;
  scaled->height = new_height;
  scaled->intensities = new float[new_width * new_height];

  float width_ratio = width / static_cast<float>(new_width);
  float height_ratio = width / static_cast<float>(new_width);

  for (int r = 0; r < new_height; ++r) {
    for (int c = 0; c < new_width; ++c) {
      float old_c = width_ratio * c; 
      float old_r = height_ratio * r; 
      (*scaled)(c, r) = Interpolate(old_c, old_r);
    }
  }

  return scaled;
}

/* ------------------------------------------------------------------------- */

Field2 *Field2::GetWhiteNoise(int width, int height) {
  // TODO: Create a new Field2 that's full of static white noise.
  Field2 *field = new Field2;
  field->intensities = new float[width * height];
  field->width = width;
  field->height = height;

  srand(time(NULL));

  for (int r = 0; r < height; ++r) {
    for (int c = 0; c < width; ++c) {
      (*field)(c, r) = rand() / static_cast<float>(RAND_MAX);
    }
  }

  return field;
}

/* ------------------------------------------------------------------------- */

Field2 *Field2::GetSmoothNoise(int width, int height, int noctaves) {
  // TODO: We'll write this together.
  Field2 *noise = new Field2(width, height);
  
  for (int i = 0; i < noctaves; ++i) {
    Field2 *white = GetWhiteNoise(width >> i, height >> i);
    Field2 *scaled_white = white->Scale(width, height);

    float factor = (1 << i) / (float) ((1 << noctaves) - 1);

    *scaled_white *= factor;
    *noise += *scaled_white;

    delete scaled_white;
    delete white;
  }

  return noise;
}

/* ------------------------------------------------------------------------- */

Field2 &Field2::operator*=(float factor) {
  for (int r = 0; r < height; ++r) {
    for (int c = 0; c < width; ++c) {
      (*this)(c, r) *= factor;
    }
  } 

  return *this;
}

/* ------------------------------------------------------------------------- */

Field2 &Field2::operator+=(const Field2 &other) {
  for (int r = 0; r < height; ++r) {
    for (int c = 0; c < width; ++c) {
      (*this)(c, r) += other(c, r);
    }
  } 

  return *this;
}

/* ------------------------------------------------------------------------- */

