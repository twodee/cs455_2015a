#ifndef IMAGE_H
#define IMAGE_H

#include <string>

namespace td {

/* ------------------------------------------------------------------------- */

class Image {
  public:
    Image(const std::string& path);
    Image(int width, int height, int nchannels);
    int GetWidth() const;
    int GetHeight() const;
    int GetChannelCount() const;
    const unsigned char *GetPixels() const;

    void WriteToFile(const std::string& path);

    unsigned char *operator()(int c, int r);
    unsigned char& operator()(int c, int r, int channel);
    const unsigned char& operator()(int c, int r, int channel) const;

  private:
    int width;
    int height;
    int nchannels;
    unsigned char *pixels;
};

/* ------------------------------------------------------------------------- */

}

#endif
