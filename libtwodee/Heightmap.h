#ifndef HEIGHTMAP_H
#define HEIGHTMAP_H

#include <string>

#include "Vector4.h"

namespace td {

/* ------------------------------------------------------------------------- */

class Heightmap {
  public:
    Heightmap(const std::string &path, float max);
    ~Heightmap();

    int GetWidth() const;
    int GetDepth() const;
    float &operator()(int x, int z);
    const float &operator()(int x, int z) const;
    float Interpolate(float x, float z) const;

    void ToAttributes(int &nvertices,
                      Vector4 *&positions,
                      Vector4 *&normals,
                      int &nfaces,
                      int *&faces);

  private:
    int size[2];
    float *heights;
};

/* ------------------------------------------------------------------------- */

}

#endif
