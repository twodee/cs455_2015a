#include <cassert>
#include <cmath>

#include "Matrix4.h"

namespace td {

/* ------------------------------------------------------------------------- */

const float PI = 3.141592653589f;

/* ------------------------------------------------------------------------- */

Matrix4::Matrix4() {
}

/* ------------------------------------------------------------------------- */

Matrix4::Matrix4(float clear) {
  data[ 0] = data[ 1] = data[ 2] = data[ 3] = clear;  
  data[ 4] = data[ 5] = data[ 6] = data[ 7] = clear;  
  data[ 8] = data[ 9] = data[10] = data[11] = clear;  
  data[12] = data[13] = data[14] = data[15] = clear;  
}

/* ------------------------------------------------------------------------- */
 
float &Matrix4::operator()(int r, int c) {
  return data[4 * c + r];
}

/* ------------------------------------------------------------------------- */

const float &Matrix4::operator()(int r, int c) const {
  return data[4 * c + r];
}

/* ------------------------------------------------------------------------- */

Matrix4 Matrix4::GetIdentity() {
  Matrix4 m(0.0f);
  m(0, 0) = 1.0f;
  m(1, 1) = 1.0f;
  m(2, 2) = 1.0f;
  m(3, 3) = 1.0f;
  return m;
}

/* ------------------------------------------------------------------------- */

Matrix4 Matrix4::GetTranslate(const Vector4& offset) {
  Matrix4 m = GetIdentity();
  m(0, 3) = offset[0];
  m(1, 3) = offset[1];
  m(2, 3) = offset[2];
  return m;
}

/* ------------------------------------------------------------------------- */

Matrix4 Matrix4::GetScale(const Vector4& factor) {
  Matrix4 m(0.0f);
  m(0, 0) = factor[0];
  m(1, 1) = factor[1];
  m(2, 2) = factor[2];
  m(3, 3) = 1.0f;
  return m;
}

/* ------------------------------------------------------------------------- */

Matrix4 Matrix4::GetRotateZ(float degrees) {
  float radians = degrees * PI / 180.0f;

  Matrix4 m(0.0f);

  // Row 0
  m(0, 0) = cosf(radians);
  m(0, 1) = -sinf(radians);

  // Row 1
  m(1, 0) = sinf(radians);
  m(1, 1) = cosf(radians);

  // Row 2
  m(2, 2) = 1.0f;

  // Row 3
  m(3, 3) = 1.0f;

  return m;
}

/* ------------------------------------------------------------------------- */

Matrix4 Matrix4::GetRotateAroundY(float degrees) {
  float radians = degrees * PI / 180.0f;
  Matrix4 m = GetIdentity();
  m(0, 0) = cos(radians);
  m(0, 2) = sin(radians);
  m(2, 0) = -sin(radians);
  m(2, 2) = cos(radians);
  return m;
}

/* ------------------------------------------------------------------------- */

Matrix4 Matrix4::GetRotateAroundX(float degrees) {
  float radians = degrees * PI / 180.0f;
  Matrix4 m = GetIdentity();
  m(1, 1) = cos(radians);
  m(1, 2) = -sin(radians);
  m(2, 1) = sin(radians);
  m(2, 2) = cos(radians);
  return m;
}

/* ------------------------------------------------------------------------- */

Matrix4 Matrix4::GetRotate(float degrees, const Vector4& axis /* normalized */) {
  float radians = (float) (degrees * PI / 180.0f);
  float sine = (float) sin(radians);
  float cosine = (float) cos(radians);
  float cos_compliment = 1.0f - cosine;

  Matrix4 m = GetIdentity();

  m(0, 0) = cos_compliment * axis[0] * axis[0] + cosine;
  m(0, 1) = cos_compliment * axis[0] * axis[1] - sine * axis[2];
  m(0, 2) = cos_compliment * axis[0] * axis[2] + sine * axis[1];

  m(1, 0) = cos_compliment * axis[1] * axis[0] + sine * axis[2];
  m(1, 1) = cos_compliment * axis[1] * axis[1] + cosine;
  m(1, 2) = cos_compliment * axis[1] * axis[2] - sine * axis[0];

  m(2, 0) = cos_compliment * axis[2] * axis[0] - sine * axis[1];
  m(2, 1) = cos_compliment * axis[2] * axis[1] + sine * axis[0];
  m(2, 2) = cos_compliment * axis[2] * axis[2] + cosine;

  return m;
}

/* ------------------------------------------------------------------------- */

Matrix4 Matrix4::GetRotate(const Vector4& from, const Vector4& to) {
  Vector4 axis = from.Cross(to);  
  axis.Normalize();
  float degrees = acos(from.Dot(to)) * 180.0f / PI;
  return GetRotate(degrees, axis);
}

/* ------------------------------------------------------------------------- */

std::ostream& operator<<(std::ostream& out, const Matrix4 &m) {
  for (int r = 0; r < 4; ++r) {
    for (int c = 0; c < 4; ++c) {
      out << m(r, c) << " ";
    }
    out << std::endl;
  }
  return out;
}

/* ------------------------------------------------------------------------- */

Vector4 Matrix4::operator*(const Vector4& v) const {
  Vector4 product;

  for (int i = 0; i < 4; ++i) {
    product[i] = (*this)(i, 0) * v[0] +
                 (*this)(i, 1) * v[1] +
                 (*this)(i, 2) * v[2] +
                 (*this)(i, 3) * v[3];
  }

  return product;
}

/* ------------------------------------------------------------------------- */

Matrix4 Matrix4::operator*(const Matrix4& that) const {
  Matrix4 product;
  for (int r = 0; r <= 3; ++r) {
    for (int c = 0; c <= 3; ++c) {
      // dot row r of this with column c of that
      product(r, c) = (*this)(r, 0) * that(0, c) +
                      (*this)(r, 1) * that(1, c) +
                      (*this)(r, 2) * that(2, c) +
                      (*this)(r, 3) * that(3, c);
    }
  }
  return product; 
}

/* ------------------------------------------------------------------------- */

Matrix4 Matrix4::GetOrtho(float l, float r, float b, float t, float n, float f) {
  Matrix4 m = Matrix4::GetIdentity(); 
  m(0, 0) = 2.0f / (r - l);
  m(1, 1) = 2.0f / (t - b);
  m(2, 2) = 2.0f / (n - f);
  m(0, 3) = -(r + l) / (r - l);
  m(1, 3) = -(t + b) / (t - b);
  m(2, 3) = (n + f) / (n - f);
  return m;
}

/* ------------------------------------------------------------------------- */

Matrix4 Matrix4::GetPerspective(float l, float r, float b, float t, float n, float f) {
  Matrix4 m = Matrix4::GetIdentity(); 

  m(0, 0) = 2.0f * n / (r - l);
  m(0, 2) = (r + l) / (r - l);

  m(1, 1) = 2.0f * n / (t - b);
  m(1, 2) = (t + b) / (t - b);

  m(2, 2) = -(f + n) / (f - n);
  m(2, 3) = -2.0f * f * n / (f - n);

  m(3, 2) = -1.0f;
  m(3, 3) = 0.0f;

  return m;
}

/* ------------------------------------------------------------------------- */

Matrix4 Matrix4::GetPerspective(float fovy, float aspect_ratio, float n, float f) {
  float t = n * tan(fovy / 2.0f * PI / 180.0f);
  float r = aspect_ratio * t;
  
  Matrix4 m = GetIdentity();
  m(0, 0) = n / r;
  m(1, 1) = n / t;
  m(2, 2) = -(f + n) / (f - n);
  m(2, 3) = -2.0f * f * n / (f - n);
  m(3, 2) = -1.0f;
  m(3, 3) = 0.0f;

  return m;
}

/* ------------------------------------------------------------------------- */

Matrix4 Matrix4::GetTranspose() const {
  Matrix4 m = GetIdentity();

  m(0, 0) = (*this)(0, 0);
  m(0, 1) = (*this)(1, 0);
  m(0, 2) = (*this)(2, 0);
  m(0, 3) = -((*this)(0, 3) * (*this)(0, 0) + (*this)(1, 3) * (*this)(1, 0) + (*this)(2, 3) * (*this)(2, 0));

  m(1, 0) = (*this)(0, 1);
  m(1, 1) = (*this)(1, 1);
  m(1, 2) = (*this)(2, 1);
  m(1, 3) = -((*this)(0, 3) * (*this)(0, 1) + (*this)(1, 3) * (*this)(1, 1) + (*this)(2, 3) * (*this)(2, 1));

  m(2, 0) = (*this)(0, 2);
  m(2, 1) = (*this)(1, 2);
  m(2, 2) = (*this)(2, 2);
  m(2, 3) = -((*this)(0, 3) * (*this)(0, 2) + (*this)(1, 3) * (*this)(1, 2) + (*this)(2, 3) * (*this)(2, 2));

  return m;
}

/* ------------------------------------------------------------------------- */

}
