#include "Camera.h"

namespace td {

/* ------------------------------------------------------------------------- */

const Matrix4& Camera::GetViewMatrix() const {
  return view_matrix;  
}

/* ------------------------------------------------------------------------- */

void Camera::LookAt(const Vector4& from,
                    const Vector4& to,
                    const Vector4& up) {
  this->from = from;
  this->to = to;

  view_matrix = Matrix4::GetIdentity();

  // What direction are we looking in world space?
  forward = to - from;
  forward.Normalize();

  // Drop that direction in view_matrix.
  view_matrix(2, 0) = -forward[0];
  view_matrix(2, 1) = -forward[1];
  view_matrix(2, 2) = -forward[2];

  // Find right axis as the vector perpendicular to both up and forward.
  right = forward.Cross(up);
  right.Normalize();

  // Drop that direction in view_matrix.
  view_matrix(0, 0) = right[0];
  view_matrix(0, 1) = right[1];
  view_matrix(0, 2) = right[2];

  // Correct up to be really, truly orthogonal.
  this->up = right.Cross(forward);
  this->up.Normalize();

  view_matrix(1, 0) = this->up[0];
  view_matrix(1, 1) = this->up[1];
  view_matrix(1, 2) = this->up[2];
  
  view_matrix = view_matrix * Matrix4::GetTranslate(Vector4(-from[0], -from[1], -from[2], 0.0f));  
}

/* ------------------------------------------------------------------------- */

void Camera::Advance(float offset) {
  from += forward * offset;  
  to += forward * offset;  
  LookAt(from, to, up);
}

/* ------------------------------------------------------------------------- */

void Camera::Strafe(float delta) {
  from += right * delta;
  to += right * delta;
  LookAt(from, to, up);
}

/* ------------------------------------------------------------------------- */

void Camera::Yaw(float degrees) {
  to = from + Matrix4::GetRotate(degrees, Vector4(0.0f, 1.0f, 0.0f, 0.0f)) * (to - from);
  LookAt(from, to, Vector4(0.0f, 1.0f, 0.0f, 0.0f));
}

/* ------------------------------------------------------------------------- */

const Vector4& Camera::GetFrom() const {
  return from;
}

/* ------------------------------------------------------------------------- */

const Vector4& Camera::GetTo() const {
  return to;
}

/* ------------------------------------------------------------------------- */

void Camera::Pitch(float degrees) {
  forward = to - from;
  to = from + Matrix4::GetRotate(degrees, right) * forward;
  forward = to - from;
  up = right.Cross(forward);
  LookAt(from, to, up);
}

/* ------------------------------------------------------------------------- */

void Camera::Roll(float degrees) {
  up = Matrix4::GetRotate(degrees, forward) * up;
  LookAt(from, to, up);
}

/* ------------------------------------------------------------------------- */

void Camera::SetHeight(float height) {
  float diff = height - from[1];
  from[1] = height;
  to[1] += diff;
  LookAt(from, to, up);
}

/* ------------------------------------------------------------------------- */

}
