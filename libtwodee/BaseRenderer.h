#ifndef BASERENDERER_H
#define BASERENDERER_H

#include "Keys.h"
#include "UtilitiesOpenGL.h"

namespace td {

/* ------------------------------------------------------------------------- */

class BaseRenderer {
  public:
    BaseRenderer();
    virtual ~BaseRenderer();

    int GetWidth() const;
    int GetHeight() const;
    float GetAspectRatio() const;

    virtual void OnSizeChanged(int width, int height);
    void SetBackgroundColor(float r, float g, float b, float a);
    virtual void OnInitialize();
    virtual void OnDraw(float deltaTime);

    virtual void OnKey(td::Keys::key_t key);
    virtual void OnLeftMouseDown(int x, int y);
    virtual void OnLeftMouseUp(int x, int y);
    virtual void OnLeftMouseDragged(int x, int y);
    virtual void OnRightMouseDown(int x, int y);
    virtual void OnRightMouseUp(int x, int y);
    virtual void OnRightMouseDragged(int x, int y);
    virtual void OnMiddleMouseDown(int x, int y);
    virtual void OnMiddleMouseUp(int x, int y);
    virtual void OnMiddleMouseDragged(int x, int y);
    virtual void OnMouseHover(int x, int y);
    virtual void OnMousePass(bool is_entering);

  private:
    float background_color[4];
    int width;
    int height;
};

/* ------------------------------------------------------------------------- */

}

#endif
