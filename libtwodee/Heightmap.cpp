#include <cassert>

#include "Heightmap.h"
#include "Image.h"

namespace td {

/* ------------------------------------------------------------------------- */

Heightmap::Heightmap(const std::string &path, float max) {
  Image image(path);
  heights = new float[image.GetWidth() * image.GetHeight()];
  size[0] = image.GetWidth();
  size[1] = image.GetHeight();
  for (int z = 0; z < this->GetDepth(); ++z) {
    for (int x = 0; x < this->GetWidth(); ++x) {
      (*this)(x, z) = image(x, z, 0) / 255.0f * max;
    }
  }
}

/* ------------------------------------------------------------------------- */

Heightmap::~Heightmap() {
  delete[] heights;
}

/* ------------------------------------------------------------------------- */

int Heightmap::GetWidth() const {
  return size[0]; 
}

/* ------------------------------------------------------------------------- */

int Heightmap::GetDepth() const {
  return size[1]; 
}

/* ------------------------------------------------------------------------- */

float &Heightmap::operator()(int x, int z) {
  if (x < 0) {
    x = 0;
  } else if (x >= GetWidth()) {
    x = GetWidth() - 1;
  }

  if (z < 0) {
    z = 0;
  } else if (z >= GetDepth()) {
    z = GetDepth() - 1;
  }

  return heights[x + z * GetWidth()];
}

/* ------------------------------------------------------------------------- */

const float &Heightmap::operator()(int x, int z) const {
  if (x < 0) {
    x = 0;
  } else if (x >= GetWidth()) {
    x = GetWidth() - 1;
  }

  if (z < 0) {
    z = 0;
  } else if (z >= GetDepth()) {
    z = GetDepth() - 1;
  }

  return heights[x + z * GetWidth()];
}

/* ------------------------------------------------------------------------- */

float Heightmap::Interpolate(float x, float z) const {
  /* int ix = (int) (x + 0.5f); */
  /* int iz = (int) (z + 0.5f); */
  /* return (*this)(ix, iz); */

#if 1
  // Find integral corner.
  int ix = (int) x;
  int iz = (int) z;

  // Find fraction away.
  float fx = x - ix;
  float fz = z - iz;

  float bl = (*this)(ix, iz);
  float br = (*this)(ix + 1, iz);
  float tl = (*this)(ix, iz + 1);
  float tr = (*this)(ix + 1, iz + 1);

  float b = fx * br + (1 - fx) * bl;
  float t = fx * tr + (1 - fx) * tl;
  float height = fz * t + (1 - fz) * b;

  return height;
#endif
}

/* ------------------------------------------------------------------------- */

void Heightmap::ToAttributes(int &nvertices,
                             Vector4 *&positions,
                             Vector4 *&normals,
                             int &nfaces,
                             int *&faces) {
  // 1. Determine the number of vertices.
  nvertices = GetWidth() * GetDepth();  
  
  // 2. Generate the list of vertex positions in the terrain. Each pixel in the
  // original image corresponds to a vertex. Its location in the image
  // determines its xz-coordinates, and its grayscale intensity determines its
  // y-coordinate.
  positions = new Vector4[nvertices];
  Vector4 *position = positions;
  for (int z = 0; z < GetDepth(); ++z) {
    for (int x = 0; x < GetWidth(); ++x) {
      *position = Vector4(x, (*this)(x, z), z, 1.0f);
      ++position;
    }
  }

  // 3. Prime the list of vertex normals to initially all have 0 length.
  normals = new Vector4[nvertices];
  for (int i = 0; i < nvertices; ++i) {
    normals[i] = Vector4(0.0f);
  }

  // 4. Determine the number of faces.
  nfaces = 2 * (GetWidth() - 1) * (GetDepth() - 1);

  // 5. Generate the list of vertex indices that connect up to form the
  // terrain's triangles. When you have identified a face's vertices, calculate
  // its normal as the crossproduct of two of its sides. Don't forget to
  // normalize. Add this face normal to each of its three vertices' normals.
  // ObjUtilities contains code that might serve as a template.
  faces = new int[nfaces * 3];
  int *face = faces;
  for (int z = 0; z < GetDepth() - 1; ++z) {
    for (int x = 0; x < GetWidth() - 1; ++x) {
      face[0] = x + z * GetWidth();
      face[1] = face[0] + 1 + GetWidth();
      face[2] = face[0] + 1;
      face[3] = face[0];
      face[4] = face[0] + GetWidth();
      face[5] = face[1];

      Vector4 a2b = positions[face[1]] - positions[face[0]];
      Vector4 a2c = positions[face[2]] - positions[face[0]];
      Vector4 face_normal = a2b.Cross(a2c);
      face_normal.Normalize();
      normals[face[0]] += face_normal;
      normals[face[1]] += face_normal;
      normals[face[2]] += face_normal;
      face += 3;

      a2b = positions[face[1]] - positions[face[0]];
      a2c = positions[face[2]] - positions[face[0]];
      face_normal = a2b.Cross(a2c);
      face_normal.Normalize();
      normals[face[0]] += face_normal;
      normals[face[1]] += face_normal;
      normals[face[2]] += face_normal;
      face += 3;
    }
  }

  // 6. Renormalize all vertex normals.
  for (int i = 0; i < nvertices; ++i) {
    normals[i].Normalize();
  }
}

/* ------------------------------------------------------------------------- */

}
