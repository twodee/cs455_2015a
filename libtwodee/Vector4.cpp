#include <cassert>
#include <cmath>

#include "Vector4.h"

namespace td {

/* ------------------------------------------------------------------------- */

Vector4::Vector4() {
}

/* ------------------------------------------------------------------------- */

Vector4::Vector4(float value) {
  xyzw[0] = xyzw[1] = xyzw[2] = xyzw[3] = value; 
}

/* ------------------------------------------------------------------------- */

Vector4::Vector4(float x, float y, float z) {
  xyzw[0] = x;
  xyzw[1] = y;
  xyzw[2] = z;
  xyzw[3] = 1.0f;
}

/* ------------------------------------------------------------------------- */

Vector4::Vector4(float x, float y, float z, float w) {
  xyzw[0] = x;
  xyzw[1] = y;
  xyzw[2] = z;
  xyzw[3] = w;
}

/* ------------------------------------------------------------------------- */

float &Vector4::operator[](int i) {
  assert(i >= 0 && i <= 3);
  return xyzw[i];
}

/* ------------------------------------------------------------------------- */

const float &Vector4::operator[](int i) const {
  assert(i >= 0 && i <= 3);
  return xyzw[i];
}

/* ------------------------------------------------------------------------- */

float Vector4::GetSquaredLength() const {
  return xyzw[0] * xyzw[0] + xyzw[1] * xyzw[1] + xyzw[2] * xyzw[2];
}

/* ------------------------------------------------------------------------- */

float Vector4::GetLength() const {
  return sqrtf(GetSquaredLength()); 
}

/* ------------------------------------------------------------------------- */

float Vector4::Dot(const Vector4& other) const {
  return xyzw[0] * other.xyzw[0] + 
         xyzw[1] * other.xyzw[1] + 
         xyzw[2] * other.xyzw[2];
}

/* ------------------------------------------------------------------------- */

Vector4 Vector4::Cross(const Vector4& other) const {
  Vector4 cross;

  cross[0] = (*this)[1] * other[2] - (*this)[2] * other[1];
  cross[1] = (*this)[2] * other[0] - (*this)[0] * other[2];
  cross[2] = (*this)[0] * other[1] - (*this)[1] * other[0];
  cross[3] = 0.0f;

  return cross; 
}

/* ------------------------------------------------------------------------- */

Vector4 &Vector4::operator+=(const Vector4 &other) {
  for (int i = 0; i < 4; ++i) {
    xyzw[i] += other.xyzw[i];
  } 
  return *this;
}

/* ------------------------------------------------------------------------- */

Vector4 &Vector4::operator*=(const Vector4 &other) {
  for (int i = 0; i < 4; ++i) {
    xyzw[i] *= other.xyzw[i];
  } 
  return *this;
}

/* ------------------------------------------------------------------------- */

Vector4 &Vector4::operator/=(const Vector4 &other) {
  for (int i = 0; i < 4; ++i) {
    xyzw[i] /= other.xyzw[i];
  } 
  return *this;
}

/* ------------------------------------------------------------------------- */

Vector4 &Vector4::operator-=(const Vector4 &other) {
  for (int i = 0; i < 4; ++i) {
    xyzw[i] -= other.xyzw[i];
  } 
  return *this;
}

/* ------------------------------------------------------------------------- */

std::ostream &operator<<(std::ostream &out, const Vector4 &v) {
  out << v[0] << " " << v[1] << " " << v[2] << " " << v[3];
  return out;
}

/* ------------------------------------------------------------------------- */

void Vector4::Normalize() {
  float length = GetLength();
  xyzw[0] /= length; 
  xyzw[1] /= length; 
  xyzw[2] /= length; 
}

/* ------------------------------------------------------------------------- */

Vector4 Vector4::operator+(const Vector4 &other) const {
  Vector4 ans(*this); 
  ans += other;
  return ans;
}

/* ------------------------------------------------------------------------- */

Vector4 Vector4::operator-(const Vector4 &other) const {
  Vector4 ans(*this); 
  ans -= other;
  return ans;
}

/* ------------------------------------------------------------------------- */

Vector4 Vector4::operator*(const Vector4 &other) const {
  Vector4 ans(*this); 
  ans *= other;
  return ans;
}

/* ------------------------------------------------------------------------- */

Vector4 Vector4::operator/(const Vector4 &other) const {
  Vector4 ans(*this); 
  ans /= other;
  return ans;
}

/* ------------------------------------------------------------------------- */

}
