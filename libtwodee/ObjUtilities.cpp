#include <fstream>
#include <sstream>

#include "ObjUtilities.h"

namespace td {

/* ------------------------------------------------------------------------- */

void ObjUtilities::SplitAttributeIndices(const std::string& attribute_indices,
                                         int &iposition,
                                         int &itexcoords) {
  int indices[3] = {-1, -1, -1};

  std::stringstream splitter(attribute_indices); 
  std::string token;
  int i = 0;
  while (getline(splitter, token, '/')) {
    std::stringstream converter(token);
    converter >> indices[i];
    --indices[i];
    ++i;
  }

  iposition = indices[0];
  itexcoords = indices[1];
}

/* ------------------------------------------------------------------------- */

void ObjUtilities::Read(const std::string& path,
                        int &nvertices,
                        float *&positions,
                        float *&texcoords,
                        Vector4 *&normals,
                        int &nfaces,
                        int *&faces) {
  std::ifstream in(path.c_str()); 

  if (in.fail()) {
    std::cerr << "bad path: " << path << std::endl;
    exit(1);
  }
 
  std::string line;
  std::string token; 

  int npositions = 0;
  int ntexcoords = 0;
  nfaces = 0;

  vertex_map attribute_indices_to_vertex; 

  while (in >> token) {
    if (token == "v") {
      ++npositions;
    } else if (token == "vt") {
      ++ntexcoords;
    } else if (token == "f") {
      ++nfaces;
      for (int i = 0; i < 3; ++i) {
        std::string attribute_indices;
        in >> attribute_indices;
        vertex_map::iterator vi = attribute_indices_to_vertex.find(attribute_indices);
        if (vi == attribute_indices_to_vertex.end()) {
          attribute_indices_to_vertex.insert(vertex_map_entry(attribute_indices, -1));
        }
      }
    }
    getline(in, line);
  }

  nvertices = attribute_indices_to_vertex.size();

  positions = new float[npositions * 3];
  texcoords = new float[ntexcoords * 2];

  faces = new int[nfaces * 3];
  Vertex *vertices = new Vertex[nvertices];

  in.clear();
  in.seekg(0, std::ios_base::beg);

  float *position = positions;
  float *texcoord = texcoords;
  int *face = faces;

  int i = 0;
  while (in >> token) {
    if (token == "v") {
      in >> position[0] >> position[1] >> position[2]; 
      position += 3;
    } else if (token == "vt") {
      in >> texcoord[0] >> texcoord[1]; 
      texcoord += 2;
    } else if (token == "f") {
      for (int fi = 0; fi < 3; ++fi) {
        std::string attribute_indices;
        in >> attribute_indices;
        face[fi] = FindVertex(attribute_indices_to_vertex, vertices, i, positions, texcoords, attribute_indices);
      }

      Vector4 a2b = vertices[face[1]].position - vertices[face[0]].position;
      Vector4 a2c = vertices[face[2]].position - vertices[face[0]].position;
      Vector4 face_normal = a2b.Cross(a2c);
      face_normal.Normalize();

      vertices[face[0]].normal += face_normal;
      vertices[face[1]].normal += face_normal;
      vertices[face[2]].normal += face_normal;

      face += 3;
    }

    getline(in, line);
  }

  for (int i = 0; i < nvertices; ++i) {
    vertices[i].normal.Normalize();
  }

  delete[] positions;
  delete[] texcoords;

  positions = new float[nvertices * 3];
  texcoords = new float[nvertices * 2];
  normals = new Vector4[nvertices];
  position = positions;
  texcoord = texcoords;
  for (int i = 0; i < nvertices; ++i) {
    position[0] = vertices[i].position[0];
    position[1] = vertices[i].position[1];
    position[2] = vertices[i].position[2];
    position += 3;
    texcoord[0] = vertices[i].texcoords[0];
    texcoord[1] = vertices[i].texcoords[1];
    texcoord += 2;
    normals[i] = vertices[i].normal;
  }

  delete[] vertices;

  in.close();
}

/* ------------------------------------------------------------------------- */

int ObjUtilities::FindVertex(vertex_map& attribute_indices_to_vertex,
                             Vertex *vertices,
                             int &nvertices_so_far,
                             float *positions,
                             float *texcoords,
                             const std::string& attribute_indices) {

  // What vertex do these attributes currently map to?
  vertex_map::iterator vi = attribute_indices_to_vertex.find(attribute_indices);

  // If none, we must create a new vertex.
  if (vi->second < 0) {
    int pi, ni, ti;
    SplitAttributeIndices(attribute_indices, pi, ti);
    vertices[nvertices_so_far].normal = Vector4(0.0f, 0.0f, 0.0f, 0.0f);
    memcpy(&vertices[nvertices_so_far].position[0], positions + pi * 3, sizeof(float) * 3);
    if (ti >= 0) {
      memcpy(&vertices[nvertices_so_far].texcoords[0], texcoords + ti * 2, sizeof(float) * 2);
    }
    vi->second = nvertices_so_far;
    ++nvertices_so_far;
  }
  
  // Let the caller know which vertex this face uses.
  return vi->second;
}

/* ------------------------------------------------------------------------- */

VertexAttributes *ObjUtilities::Read(const std::string &path) {
  int nvertices;
  float *positions;
  Vector4 *normals;
  float *texcoords;
  int nfaces;
  int *faces;
  Read(path, nvertices, positions, texcoords, normals, nfaces, faces);

  VertexAttributes *attributes = new VertexAttributes();
  attributes->AddAttribute("position", nvertices, 3, positions);
  attributes->AddAttribute("normal", nvertices, 4, &normals[0][0]);
  attributes->AddAttribute("texcoord", nvertices, 2, texcoords);
  attributes->AddIndices(nfaces * 3, faces);

  return attributes;
}

/* ------------------------------------------------------------------------- */

}
