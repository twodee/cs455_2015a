#include <cstring>
#include <iostream>

#include "glut_utilities.h"
#include "Keys.h"
#include "MessagedException.h"
#include "UtilitiesOpenGL.h"

#ifdef __APPLE__
#include <glut.h>
#else
#include <GL/freeglut.h>
#endif

/* ------------------------------------------------------------------------- */

td::BaseRenderer *renderer = NULL;

enum mouse_button_t {
  MOUSE_LEFT,
  MOUSE_RIGHT,
  MOUSE_MIDDLE,
  MOUSE_NONE
};

mouse_button_t mouse_button;

bool is_warped;

/* ------------------------------------------------------------------------- */
/* FUNCTION PROTOTYPES                                                       */
/* ------------------------------------------------------------------------- */

void on_cleanup();
void on_mouse(int button,
              int state,
              int x,
              int y);
void on_mouse_dragged(int x,
                      int y);
void on_key(unsigned char key,
            int x,
            int y);
void on_special_key(int key,
                    int x,
                    int y);
void on_mouse(int button,
              int x,
              int y);
void on_size_changed(int width,
                     int height);
void on_draw();
void on_mouse_hover(int x, int y);
void on_mouse_pass(int state);

int then;

/* ------------------------------------------------------------------------- */

void glut_render(td::BaseRenderer *given_renderer) {
  renderer = given_renderer;
  atexit(on_cleanup);

  glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
  glutInitWindowSize(512, 512);
  glutInitWindowPosition(50, 50);

  int argc = 1;
  char *argv[1];
  argv[0] = (char *) malloc(20 * sizeof(char));
  strcpy(argv[0], "cs455.exe");
  glutInit(&argc, argv);
  then = glutGet(GLUT_ELAPSED_TIME);

  glutCreateWindow("...");
  glutDisplayFunc(on_draw);
  glutIdleFunc(on_draw);
  glutReshapeFunc(on_size_changed);
  glutKeyboardFunc(on_key);
  glutSpecialFunc(on_special_key);
  glutMouseFunc(on_mouse);
  glutMotionFunc(on_mouse_dragged);
  glutPassiveMotionFunc(on_mouse_hover);
  glutEntryFunc(on_mouse_pass);

  try {
    td::OpenGL::SetupGLEW();
    renderer->OnInitialize();
    glutMainLoop();
  } catch (td::MessagedException e) {
    std::cerr << "EXCEPTION: " << e.what() << std::endl;
  }
}

/* ------------------------------------------------------------------------- */

void on_cleanup() {
  delete renderer;
}

/* ------------------------------------------------------------------------- */

void on_draw() {
  int now = glutGet(GLUT_ELAPSED_TIME);
  renderer->OnDraw((now - then) / 1000.0f);
  then = now;
  glutSwapBuffers();
}

/* ------------------------------------------------------------------------- */

void on_size_changed(int width,
                     int height) {
  renderer->OnSizeChanged(width, height);
  glutPostRedisplay();
}

/* ------------------------------------------------------------------------- */

void on_mouse(int button,
              int state,
              int x,
              int y) {
  y = renderer->GetHeight() - y;
  if (state == GLUT_DOWN) {
    if (button == GLUT_LEFT_BUTTON) {
      renderer->OnLeftMouseDown(x, y);
      mouse_button = MOUSE_LEFT;
    } else if (button == GLUT_RIGHT_BUTTON) {
      renderer->OnRightMouseDown(x, y);
      mouse_button = MOUSE_RIGHT;
    } else if (button == GLUT_MIDDLE_BUTTON) {
      renderer->OnMiddleMouseDown(x, y);
      mouse_button = MOUSE_MIDDLE;
    }
  } else if (state == GLUT_UP) {
    if (button == GLUT_LEFT_BUTTON) {
      renderer->OnLeftMouseUp(x, y);
    } else if (button == GLUT_RIGHT_BUTTON) {
      renderer->OnRightMouseUp(x, y);
    } else if (button == GLUT_MIDDLE_BUTTON) {
      renderer->OnMiddleMouseUp(x, y);
    }
    mouse_button = MOUSE_NONE;
  }

  glutPostRedisplay();
}

/* ------------------------------------------------------------------------- */

void on_mouse_dragged(int x,
                      int y) {
  y = renderer->GetHeight() - y;

  if (mouse_button == MOUSE_LEFT) {
    renderer->OnLeftMouseDragged(x, y);
  } else if (mouse_button == MOUSE_RIGHT) {
    renderer->OnRightMouseDragged(x, y);
  } else if (mouse_button == MOUSE_MIDDLE) {
    renderer->OnMiddleMouseDragged(x, y);
  }

  glutPostRedisplay();
}

/* ------------------------------------------------------------------------- */

void on_key(unsigned char key,
            int x,
            int y) {
  if (key == 27) {
    exit(0);
  } else {
    renderer->OnKey((td::Keys::key_t) key);
    glutPostRedisplay();
  }
}

/* ------------------------------------------------------------------------- */

void on_special_key(int glut_key,
                    int x,
                    int y) {
  td::Keys::key_t key;

  if (glut_key == GLUT_KEY_PAGE_UP) {
    key = td::Keys::PAGEUP;
  } else if (glut_key == GLUT_KEY_UP) {
    key = td::Keys::UP;
  } else if (glut_key == GLUT_KEY_DOWN) {
    key = td::Keys::DOWN;
  } else if (glut_key == GLUT_KEY_LEFT) {
    key = td::Keys::LEFT;
  } else if (glut_key == GLUT_KEY_RIGHT) {
    key = td::Keys::RIGHT;
  } else {
    return;
  }

  renderer->OnKey(key);

  glutPostRedisplay();
}

/* ------------------------------------------------------------------------- */

void on_mouse_hover(int x, int y) {
  if (is_warped) {
    is_warped = false;
    return;
  }

  y = renderer->GetHeight() - y;
  renderer->OnMouseHover(x, y);
  glutPostRedisplay();
}

/* ------------------------------------------------------------------------- */

void on_mouse_pass(int state) {
  renderer->OnMousePass(state == GLUT_ENTERED);
}

/* ------------------------------------------------------------------------- */

void glut_cursor_off() {
  glutSetCursor(GLUT_CURSOR_NONE); 
}

/* ------------------------------------------------------------------------- */

void glut_cursor_on() {
  glutSetCursor(GLUT_CURSOR_RIGHT_ARROW); 
}

/* ------------------------------------------------------------------------- */

void glut_cursor_warp(int x, int y) {
  is_warped = true;
  glutWarpPointer(x, y); 
}

/* ------------------------------------------------------------------------- */

