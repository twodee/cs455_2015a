#include <iostream>

#include "BaseRenderer.h"

namespace td {

/* ------------------------------------------------------------------------- */

BaseRenderer::BaseRenderer() {
}

/* ------------------------------------------------------------------------- */

BaseRenderer::~BaseRenderer() {
}

/* ------------------------------------------------------------------------- */

void BaseRenderer::OnDraw(float deltaTime) {
  glViewport(0, 0, width, height);
  glClearColor(background_color[0], background_color[1], background_color[2], background_color[3]);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

/* ------------------------------------------------------------------------- */

void BaseRenderer::OnInitialize() {
}

/* ------------------------------------------------------------------------- */

int BaseRenderer::GetWidth() const {
  return width;
}

/* ------------------------------------------------------------------------- */

int BaseRenderer::GetHeight() const {
  return height;
}

/* ------------------------------------------------------------------------- */

void BaseRenderer::OnSizeChanged(int width, int height) {
  this->width = width;
  this->height = height;
}

/* ------------------------------------------------------------------------- */

void BaseRenderer::SetBackgroundColor(float r, float g, float b, float a) {
  background_color[0] = r;
  background_color[1] = g;
  background_color[2] = b;
  background_color[3] = a;
}

/* ------------------------------------------------------------------------- */

void BaseRenderer::OnKey(td::Keys::key_t key) {
  std::cout << "key: " << key << std::endl;
}

/* ------------------------------------------------------------------------- */

void BaseRenderer::OnLeftMouseDown(int x, int y) {
  std::cout << "left down at " << x << ", " << y << std::endl;
}

/* ------------------------------------------------------------------------- */

void BaseRenderer::OnLeftMouseUp(int x, int y) {
  std::cout << "left up at " << x << ", " << y << std::endl;
}

/* ------------------------------------------------------------------------- */

void BaseRenderer::OnLeftMouseDragged(int x, int y) {
  std::cout << "left dragged at " << x << ", " << y << std::endl;
}

/* ------------------------------------------------------------------------- */

void BaseRenderer::OnRightMouseDown(int x, int y) {
  std::cout << "right down at " << x << ", " << y << std::endl;
}

/* ------------------------------------------------------------------------- */

void BaseRenderer::OnRightMouseUp(int x, int y) {
  std::cout << "right up at " << x << ", " << y << std::endl;
}

/* ------------------------------------------------------------------------- */

void BaseRenderer::OnRightMouseDragged(int x, int y) {
  std::cout << "right dragged at " << x << ", " << y << std::endl;
}

/* ------------------------------------------------------------------------- */

void BaseRenderer::OnMiddleMouseDown(int x, int y) {
  std::cout << "middle down at " << x << ", " << y << std::endl;
}

/* ------------------------------------------------------------------------- */

void BaseRenderer::OnMiddleMouseUp(int x, int y) {
  std::cout << "middle up at " << x << ", " << y << std::endl;
}

/* ------------------------------------------------------------------------- */

void BaseRenderer::OnMiddleMouseDragged(int x, int y) {
  std::cout << "middle dragged at " << x << ", " << y << std::endl;
}

/* ------------------------------------------------------------------------- */

float BaseRenderer::GetAspectRatio() const {
  return GetWidth() / (float) GetHeight();
}

/* ------------------------------------------------------------------------- */

void BaseRenderer::OnMouseHover(int x, int y) {
}

/* ------------------------------------------------------------------------- */

void BaseRenderer::OnMousePass(bool is_entering) {
}

/* ------------------------------------------------------------------------- */

}
