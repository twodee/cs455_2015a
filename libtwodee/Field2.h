#ifndef FIELD2_H
#define FIELD2_H

class Field2 {
  public:
    Field2();
    Field2(int width, int height);
    ~Field2();

    const float *GetIntensities() const;
    int GetWidth() const;
    int GetHeight() const;
    float Interpolate(float x, float y) const;

    void WriteToPGM(const std::string &path);

    Field2 &operator+=(const Field2 &other);

    Field2 &operator*=(float factor);

    float &operator()(int c, int r);
    const float &operator()(int c, int r) const;

    Field2 *Scale(int new_width, int new_height);

    static Field2 *GetWhiteNoise(int width, int height);
    static Field2 *GetSmoothNoise(int width, int height, int noctaves);

  private:
    int width;
    int height;
    float *intensities;
};

#endif
