#ifndef FRAMEBUFFEROBJECT_H
#define FRAMEBUFFEROBJECT_H

#include "Configuration.h"

#include "Texture.h"
#include "UtilitiesOpenGL.h"

namespace td {

/* ------------------------------------------------------------------------- */

class FramebufferObject {
  public:
    FramebufferObject();
    FramebufferObject(const Texture *texture);

    void Bind();
    void Unbind();
    /* void Delete(); */

    // Assumes bound.
    void AddColor(const Texture *texture);
    void AddDepth(const Texture *texture);
    void AddDepth();

    int GetWidth() const;
    int GetHeight() const;

  private:
    void Initialize();

    // Framebuffer object ID.
    GLuint framebuffer_id;
    GLuint depth_id;
    std::vector<const Texture *> color_attachments;

    int width;
    int height;
};

/* ------------------------------------------------------------------------- */

}
#endif
