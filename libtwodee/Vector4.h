#ifndef VECTOR4_H
#define VECTOR4_H

#include <iostream>

namespace td {

/* ------------------------------------------------------------------------- */

class Vector4 {
  public:
    Vector4();
    Vector4(float value);
    Vector4(float x, float y, float z);
    Vector4(float x, float y, float z, float w);

    void Normalize();

    float &operator[](int i);
    const float &operator[](int i) const;

    float GetSquaredLength() const;
    float GetLength() const;
    float Dot(const Vector4& other) const;
    Vector4 Cross(const Vector4& other) const;

    Vector4 &operator+=(const Vector4 &other);
    Vector4 &operator*=(const Vector4 &other);
    Vector4 &operator/=(const Vector4 &other);
    Vector4 &operator-=(const Vector4 &other);
    Vector4 operator+(const Vector4 &other) const;
    Vector4 operator-(const Vector4 &other) const;
    Vector4 operator*(const Vector4 &other) const;
    Vector4 operator/(const Vector4 &other) const;

  private:
    float xyzw[4];
};

/* ------------------------------------------------------------------------- */

std::ostream &operator<<(std::ostream &out, const Vector4 &v);

/* ------------------------------------------------------------------------- */

}

#endif
