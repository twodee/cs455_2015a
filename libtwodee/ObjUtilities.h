#ifndef OBJUTILITIES_H
#define OBJUTILITIES_H

#include <map>
#include <string>

#include "Vector4.h"
#include "VertexAttributes.h"

namespace td {

/* ------------------------------------------------------------------------- */

class ObjUtilities {
  typedef std::map<std::string, int> vertex_map;
  typedef std::pair<std::string, int> vertex_map_entry;

  struct Vertex {
    Vector4 position;
    Vector4 texcoords;
    Vector4 normal;
  };

  public:
    static void Read(const std::string& path,
                     int &nvertices,
                     float *&positions,
                     float *&texcoords,
                     Vector4 *&normals,
                     int &nfaces,
                     int *&faces);
    static VertexAttributes *Read(const std::string &path);

  private:
    static void SplitAttributeIndices(const std::string& attribute_indices,
                                      int &iposition,
                                      int &itexcoords);
    static int FindVertex(vertex_map& attribute_indices_to_vertex,
                          Vertex *vertices,
                          int &nvertices_so_far,
                          float *positions,
                          float *texcoords,
                          const std::string& attribute_indices);
};

/* ------------------------------------------------------------------------- */

}

#endif
