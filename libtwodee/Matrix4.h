#ifndef MATRIX4_H
#define MATRIX4_H

#include <iostream>

#include "Vector4.h"

namespace td {

/* ------------------------------------------------------------------------- */

class Matrix4 {
  public:
    Matrix4(); 
    Matrix4(float clear); 

    float &operator()(int r, int c);
    const float &operator()(int r, int c) const;
    Matrix4 GetTranspose() const;

    static Matrix4 GetIdentity();
    static Matrix4 GetTranslate(const Vector4& offset);
    static Matrix4 GetScale(const Vector4& factor);
    static Matrix4 GetRotateZ(float degrees);
    static Matrix4 GetRotateAroundX(float degrees);
    static Matrix4 GetRotateAroundY(float degrees);
    static Matrix4 GetRotate(float degrees, const Vector4& axis);
    static Matrix4 GetRotate(const Vector4& from, const Vector4& to);
    static Matrix4 GetOrtho(float l, float r, float b, float t, float n, float f);
    static Matrix4 GetPerspective(float l, float r, float b, float t, float n, float f);
    static Matrix4 GetPerspective(float fovy, float aspect_ratio, float n, float f);

    Vector4 operator*(const Vector4& other) const;
    Matrix4 operator*(const Matrix4& other) const;

  private:
    float data[16];
};

/* ------------------------------------------------------------------------- */

std::ostream& operator<<(std::ostream& out, const Matrix4 &m);

/* ------------------------------------------------------------------------- */

}

#endif
