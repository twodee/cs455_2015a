#include <cmath>
#include <iostream>
#include <fstream>

#include "BaseRenderer.h"
#include "glut_utilities.h"
#include "Image.h"
#include "Matrix4.h"
#include "ObjUtilities.h"
#include "VertexArray.h"

using namespace td;

/* ------------------------------------------------------------------------- */

class EighthRenderer : public BaseRenderer {
  public:
    void OnDraw(float delta);
    void OnInitialize();
    void OnKey(Keys::key_t key);
    void OnSizeChanged(int width, int height);
    void OnLeftMouseDown(int x, int y);
    void OnLeftMouseDragged(int x, int y);
    void OnLeftMouseUp(int x, int y);

  private:
    Vector4 ProjectOntoTrackball(int x_pixel, int y_pixel) const;

    VertexAttributes *plane_attributes;
    ShaderProgram *shader_program;
    VertexArray *plane;
    Texture *texture;

    Vector4 fore;
    Matrix4 xform;
};

/* ------------------------------------------------------------------------- */

Vector4 EighthRenderer::ProjectOntoTrackball(int x_pixel, int y_pixel) const {
  Vector4 v_pixel = Vector4(x_pixel, y_pixel, 0.0f, 1.0f); 
  Vector4 v_ball =
    Matrix4::GetTranslate(Vector4(-1.0f, -1.0f, 0.0f)) *
    Matrix4::GetScale(Vector4(2.0f / GetWidth(), 2.0f / GetHeight(), 1.0f)) *
    v_pixel;

  float z_squared = 1.0f - v_ball[0] * v_ball[0] - v_ball[1] * v_ball[1];
  if (z_squared < 0.0f) {
    z_squared = 0.0f;
  }

  v_ball[2] = sqrtf(z_squared);
  v_ball.Normalize();

  return v_ball;
}

/* ------------------------------------------------------------------------- */

void EighthRenderer::OnLeftMouseDown(int x, int y) {
  fore = ProjectOntoTrackball(x, y);
}

/* ------------------------------------------------------------------------- */

void EighthRenderer::OnLeftMouseDragged(int x, int y) {
  Vector4 aft = ProjectOntoTrackball(x, y);

  float theta = acos(fore.Dot(aft));
  if (fabs(theta) > 1.0e-6f) {
    Vector4 axis = fore.Cross(aft);
    axis.Normalize();

    Matrix4 rotation = Matrix4::GetRotate(theta * 180.0f / 3.14159265358979323846264f, axis);

    shader_program->Bind();  
    shader_program->SetUniform("xform", rotation * xform);
    shader_program->Unbind();  
  }
}

/* ------------------------------------------------------------------------- */

void EighthRenderer::OnLeftMouseUp(int x, int y) {
  Vector4 aft = ProjectOntoTrackball(x, y);

  float theta = acos(fore.Dot(aft));
  if (fabs(theta) > 1.0e-6f) {
    Vector4 axis = fore.Cross(aft);
    axis.Normalize();

    Matrix4 rotation = Matrix4::GetRotate(theta * 180.0f / 3.14159265358979323846264f, axis);
    xform = rotation * xform;

    shader_program->Bind();  
    shader_program->SetUniform("xform", xform);
    shader_program->Unbind();  
  }
}

/* ------------------------------------------------------------------------- */

void EighthRenderer::OnDraw(float delta) {
  BaseRenderer::OnDraw(delta);

  shader_program->Bind();  
  shader_program->SetUniform("tex", *texture);
  plane->Bind();
  plane->DrawIndexed(GL_TRIANGLES);
  plane->Unbind();
  shader_program->Unbind();
}

/* ------------------------------------------------------------------------- */

void EighthRenderer::OnInitialize() {
  SetBackgroundColor(0.0f / 255, 0.0f / 255, 0.0f / 255, 1.0f);   

  glEnable(GL_DEPTH_TEST);

  int npositions = 4;
  int nfaces = 2;
  float positions[] = {
    -1.0f, -1.0f, 0.0f,
     1.0f, -1.0f, 0.0f,
    -1.0f,  1.0f, 0.0f,
     1.0f,  1.0f, 0.0f
  };
  float texcoords[] = {
    0.0f, 0.0f,
    4.0f, 0.0f,
    0.0f, 4.0f,
    4.0f, 4.0f
  };
  int faces[] = {
    0, 1, 3,
    0, 3, 2
  };
  Vector4 normals[] = {
    Vector4(0.0f, 0.0f, 1.0f),
    Vector4(0.0f, 0.0f, 1.0f),
    Vector4(0.0f, 0.0f, 1.0f),
    Vector4(0.0f, 0.0f, 1.0f)
  };

  Image image("bowie.ppm");
  glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
  texture = new Texture(0);
  /* texture->Wrap(Texture::CLAMP_TO_BORDER); */

  texture->Upload(image.GetWidth(), image.GetHeight(), image.GetPixels());

  // Ship positions off to GPU.
  plane_attributes = new VertexAttributes();
  plane_attributes->AddAttribute("position", npositions, 3, positions);
  plane_attributes->AddAttribute("texcoords", npositions, 2, texcoords);
  plane_attributes->AddAttribute("normal", npositions, 4, &normals[0][0]);
  plane_attributes->AddIndices(nfaces * 3, faces);

  shader_program = ShaderProgram::FromFiles(SHADERS_DIR "/v.glsl", SHADERS_DIR "/f.glsl");

  xform = Matrix4::GetIdentity();
  shader_program->Bind();
  shader_program->SetUniform("xform", xform);
  shader_program->Unbind();  

  plane = new VertexArray(*shader_program, *plane_attributes);
}

/* ------------------------------------------------------------------------- */

void EighthRenderer::OnSizeChanged(int width, int height) {
  BaseRenderer::OnSizeChanged(width, height);

  Matrix4 projection;
  float side = 1.1f;
  if (GetAspectRatio() > 1.0f) {
    projection = Matrix4::GetOrtho(GetAspectRatio() * -side, GetAspectRatio() * side, -side, side, -10.0f, 10.0f);
  } else {
    projection = Matrix4::GetOrtho(-side, side, -side / GetAspectRatio(), side / GetAspectRatio(), -10.0f, 10.0f);
  }

  shader_program->Bind();
  shader_program->SetUniform("projection", projection);
  shader_program->Unbind();  
}

/* ------------------------------------------------------------------------- */

void EighthRenderer::OnKey(Keys::key_t key) {
  switch (key) {
    case 'w':
      glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
      break;
    case 'W':
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
      break;
    default:
      BaseRenderer::OnKey(key);
      break;  
  }
}

/* ------------------------------------------------------------------------- */

int main(int argc, char **argv) {
  EighthRenderer *renderer = new EighthRenderer();
  glut_render(renderer); 
  return 0;
}

/* ------------------------------------------------------------------------- */

// eighth
